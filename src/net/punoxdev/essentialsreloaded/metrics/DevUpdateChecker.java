package net.punoxdev.essentialsreloaded.metrics;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.scheduler.BukkitScheduler;

import net.punoxdev.essentialsreloaded.Main;

public class DevUpdateChecker
{
  private static boolean update = false;
  public static String check = readURL("https://api.spigotmc.org/legacy/update.php?resource=65378").substring(1);
  
  public static void check()
  {
    String check = readURL("https://api.spigotmc.org/legacy/update.php?resource=65378").substring(1);
    if (check.equalsIgnoreCase(Main.getDevVersion())) {
      setUpdate(false);
    } else {
      setUpdate(true);
    }
  }
  
  public static void checkDelay(int seconds)
  {
    Bukkit.getScheduler().scheduleAsyncRepeatingTask(net.punoxdev.essentialsreloaded.Main.getInstance(), new Runnable()
    {
      public void run() {}
    }, seconds * 20, seconds * 20);
  }
  
  private static String readURL(String URL)
  {
    String re = "";
    try
    {
      URL url = new URL(URL);
      Reader is = new InputStreamReader(url.openStream());
      BufferedReader in = new BufferedReader(is);
      String s;
      while ((s = in.readLine()) != null) {
        re = re + " " + s;
      }
      in.close();
    }
    catch (Exception e)
    {
      setUpdate(false);
    }
    return re;
  }
  
  public static boolean isUpdate()
  {
    return update;
  }
  
  private static void setUpdate(boolean state)
  {
    update = state;
  }
}
