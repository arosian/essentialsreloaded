package net.punoxdev.essentialsreloaded.metrics;

import org.bukkit.Bukkit;

import net.punoxdev.essentialsreloaded.Main;

public class StartupUtilities {

    public static void sendStartupMessage() {
        Bukkit.getConsoleSender().sendMessage("");
        Bukkit.getConsoleSender().sendMessage("");
        Bukkit.getConsoleSender().sendMessage("§7§l[]========[ §e§lEssentialsReloaded " + Main.getVersion() + " §7§l]========[]");
        Bukkit.getConsoleSender().sendMessage("");
        Bukkit.getConsoleSender().sendMessage("§e§l SKILLAPI LEVEL§7 * §40.4.1");
        Bukkit.getConsoleSender().sendMessage("§c§l PROGRAMMED BY PUNOXDEV");
        Bukkit.getConsoleSender().sendMessage("§c§l MAINTAINED BY 'becausedesignmatters by Andreas Rosian'");
        Bukkit.getConsoleSender().sendMessage("§c§l get more informations under https://becausedesignmatters.at/essreloaded/'");
        Bukkit.getConsoleSender().sendMessage("");
        Bukkit.getConsoleSender().sendMessage("§7§l[]========[ §e§lEssentialsReloaded " + Main.getVersion() + " §7§l]========[]");
        Bukkit.getConsoleSender().sendMessage("");
    }
}
