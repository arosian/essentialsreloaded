package net.punoxdev.essentialsreloaded.repositories;

import lombok.Getter;
import lombok.Setter;
import net.punoxdev.essentialsreloaded.entities.Command;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CommandRepository {

    private List<List<Command>> commands = new ArrayList<>();

    public List<List<Command>> getCommands() {
        return commands;
    }

    public void setCommands(List<List<Command>> commandsToset) {
        this.commands = commands;
    }

    public void addCommands(List<Command> commandsToAdd) {
        this.commands.add(commandsToAdd);
    }

    public void removeCommands(List<Command> commandsToRemove) {
        this.commands.remove(commandsToRemove);
    }
}
