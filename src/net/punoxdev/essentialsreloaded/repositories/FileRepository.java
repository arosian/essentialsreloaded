package net.punoxdev.essentialsreloaded.repositories;

import lombok.Getter;
import lombok.Setter;
import net.punoxdev.essentialsreloaded.api.FileAPI;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

@Getter
@Setter
public class FileRepository {

    private File settingsFile;
    private YamlConfiguration settingsConfiguration;

    private File coinsFile;
    private YamlConfiguration coinsConfiguration;

    private File warpsFile;
    private YamlConfiguration warpsConfiguration;

    private File worldsFile;
    private YamlConfiguration worldsConfiguration;

    private File mysqlFile;
    private YamlConfiguration mysqlConfiguration;

    private File messagesFile;
    private YamlConfiguration messagesConfiguration;

    public FileRepository() {
        this.settingsFile = FileAPI.getFile("settings.yml");
        this.settingsConfiguration = FileAPI.getYamlConfiguration("settings.yml");
        this.coinsFile = FileAPI.getFile("coins.yml");
        this.coinsConfiguration = FileAPI.getYamlConfiguration("coins.yml");
        this.warpsFile = FileAPI.getFile("warps.yml");
        this.warpsConfiguration = FileAPI.getYamlConfiguration("warps.yml");
        this.worldsFile = FileAPI.getFile("worlds.yml");
        this.worldsConfiguration = FileAPI.getYamlConfiguration("worlds.yml");
        this.mysqlFile = FileAPI.getFile("mysql.yml");
        this.mysqlConfiguration = FileAPI.getYamlConfiguration("mysql.yml");
        this.messagesFile = FileAPI.getFile("messages.yml");
        this.messagesConfiguration = FileAPI.getYamlConfiguration("messages.yml");
    }

    public File getSettingsFile() {
        return settingsFile;
    }

    public void setSettingsFile(File settingsFile) {
        this.settingsFile = settingsFile;
    }

    public YamlConfiguration getSettingsConfiguration() {
        return settingsConfiguration;
    }

    public void setSettingsConfiguration(YamlConfiguration settingsConfiguration) {
        this.settingsConfiguration = settingsConfiguration;
    }

    public File getCoinsFile() {
        return coinsFile;
    }

    public void setCoinsFile(File coinsFile) {
        this.coinsFile = coinsFile;
    }

    public YamlConfiguration getCoinsConfiguration() {
        return coinsConfiguration;
    }

    public void setCoinsConfiguration(YamlConfiguration coinsConfiguration) {
        this.coinsConfiguration = coinsConfiguration;
    }

    public File getWarpsFile() {
        return warpsFile;
    }

    public void setWarpsFile(File warpsFile) {
        this.warpsFile = warpsFile;
    }

    public YamlConfiguration getWarpsConfiguration() {
        return warpsConfiguration;
    }

    public void setWarpsConfiguration(YamlConfiguration warpsConfiguration) {
        this.warpsConfiguration = warpsConfiguration;
    }

    public File getWorldsFile() {
        return worldsFile;
    }

    public void setWorldsFile(File worldsFile) {
        this.worldsFile = worldsFile;
    }

    public YamlConfiguration getWorldsConfiguration() {
        return worldsConfiguration;
    }

    public void setWorldsConfiguration(YamlConfiguration worldsConfiguration) {
        this.worldsConfiguration = worldsConfiguration;
    }

    public File getMysqlFile() {
        return mysqlFile;
    }

    public void setMysqlFile(File mysqlFile) {
        this.mysqlFile = mysqlFile;
    }

    public YamlConfiguration getMysqlConfiguration() {
        return mysqlConfiguration;
    }

    public void setMysqlConfiguration(YamlConfiguration mysqlConfiguration) {
        this.mysqlConfiguration = mysqlConfiguration;
    }

    public File getMessagesFile() {
        return messagesFile;
    }

    public void setMessagesFile(File messagesFile) {
        this.messagesFile = messagesFile;
    }

    public YamlConfiguration getMessagesConfiguration() {
        return messagesConfiguration;
    }

    public void setMessagesConfiguration(YamlConfiguration messagesConfiguration) {
        this.messagesConfiguration = messagesConfiguration;
    }
}