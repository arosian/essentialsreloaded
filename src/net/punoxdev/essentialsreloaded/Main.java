package net.punoxdev.essentialsreloaded;

import lombok.Getter;
import lombok.Setter;
import net.punoxdev.essentialsreloaded.api.CoinsAPI;
import net.punoxdev.essentialsreloaded.api.CommandAPI;
import net.punoxdev.essentialsreloaded.api.HomesAPI;
import net.punoxdev.essentialsreloaded.commands.*;
import net.punoxdev.essentialsreloaded.commands.WorldSystem.WorldCMD;
import net.punoxdev.essentialsreloaded.entities.Command;
import net.punoxdev.essentialsreloaded.events.EventListener;
import net.punoxdev.essentialsreloaded.manager.MySQL;
import net.punoxdev.essentialsreloaded.manager.SQLEventListener;
import net.punoxdev.essentialsreloaded.metrics.DevUpdateChecker;
import net.punoxdev.essentialsreloaded.metrics.MainUpdateChecker;
import net.punoxdev.essentialsreloaded.metrics.Metrics;
import net.punoxdev.essentialsreloaded.metrics.StartupUtilities;
import net.punoxdev.essentialsreloaded.repositories.CommandRepository;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;

@Getter
@Setter
public class Main extends JavaPlugin {

    private FileRepository fileRepository = new FileRepository();
    private CommandRepository commandRepository = new CommandRepository();

    MySQL mySQL;

    public static Main instance;
    private String pluginPath;
    
    public FileRepository getFileRepository() {
        return fileRepository;
    }

    public CommandRepository getCommandRepository() {
        return commandRepository;
    }

    public String getPluginPath() {
        return this.pluginPath;
    }

    public void setPluginPath(String pluginPath) {
        this.pluginPath = pluginPath;
    }

    public Boolean isMySQLEnabled() {
        return fileRepository.getMysqlConfiguration().getBoolean("mysql.enable");
    }

    public static Main getInstance() {
        return instance;
    }

    // 1.9
    
    //Dont forget!!
    private static Boolean isDevVersion = false;

    public static String getCurrentVersion() {
    	if(isDevVersion) {
    		return getDevVersion();
    	} else {
    		return getVersion();
    	}
    }
    
    public static String getVersion() {
        return "2.0.2 SECURITY";
    }

    public static String getDevVersion() {
        return "2.0 DIV";
    }

    public MySQL getMySQL() {
        return mySQL;
    }

    // 2.0

    @Override
    public void onEnable() {
    	instance = this;
        setPluginPath("plugins/EssentialsReloaded");
        loadConfig();
        HomesAPI.loadHomes();
        MainUpdateChecker.check();
        DevUpdateChecker.check();
        
        StartupUtilities.sendStartupMessage();
        
        commandRepository.addCommands(
                Arrays.asList(
                        // Default Commands -> Always activated
                        new Command("fly", "default", true, new FlyCMD()),
                        new Command("help", "default", true, new HelpCMD()),
                        new Command("updatelog", "default", true, new UpdateLogCMD()),
                        //new Command("overview", "default", true, new OverviewCMD()),
                        //new Command("ov", "default", true, new OverviewCMD()),
                        // PaymentSystem
                        new Command("coinssettings", "PaymentSystem", false, new CoinsSettingsCMD()),
                        new Command("coins", "PaymentSystem", false, new CoinsCMD()),
                        new Command("money", "PaymentSystem", false, new CoinsCMD()),
                        new Command("geld", "PaymentSystem", false, new CoinsCMD()),
                        new Command("dollar", "PaymentSystem", false, new CoinsCMD()),
                        new Command("pay", "PaymentSystem", false, new PayCMD()),
                        // GameMode
                        new Command("gamemode", "GameMode", false, new GameModeCMD()),
                        new Command("gm", "GameMode", false, new GameModeCMD()),
                        // Broadcast
                        new Command("broadcast", "Broadcast", false, new BroadcastCMD()),
                        new Command("bc", "Broadcast", false, new BroadcastCMD()),
                        // ChatClear
                        new Command("chatclear", "ChatClear", false, new ChatClearCMD()),
                        new Command("clearchat", "ChatClear", false, new ChatClearCMD()),
                        new Command("cc", "ChatClear", false, new ChatClearCMD()),
                        // Feed & Heal
                        new Command("heal", "Feed & Heal", false, new HealCMD()),
                        new Command("feed", "Feed & Heal", false, new FeedCMD()),
                        // Home & SetHome
                        new Command("home", "Home & SetHome", false, new HomeCMD()),
                        new Command("sethome", "Home & SetHome", false, new SetHomeCMD()),
                        new Command("homes", "Home & SetHome", false, new HomesCMD()),
                        new Command("delhome", "Home & SetHome", false, new DelHomeCMD()),
                        // KickSystem
                        new Command("kick", "KickSystem", false, new KickCMD()),
                        // Vanish
                        new Command("vanish", "Vanish", false, new VanishCMD()),
                        new Command("v", "Vanish", false, new VanishCMD()),
                        // Invsee
                        new Command("invsee", "Invsee", false, new InvseeCMD()),
                        // Teleport
                        new Command("tp", "Teleport", false, new TeleportCMD()),
                        new Command("tphere", "Teleport", false, new TeleportOtherCMD()),
                        new Command("tpo", "Teleport", false, new TeleportCMD()),
                        // TPA
                        new Command("tpa", "TPA", false, new TeleportOtherCMD()),
                        new Command("tpaccept", "TPA", false, new TeleportOtherCMD()),
                        new Command("tpadeny", "TPA", false, new TeleportOtherCMD()),
                        // WarpSystem
                        new Command("warp", "WarpSystem", false, new WarpCMD()),
                        new Command("setwarp", "WarpSystem", false, new SetWarpCMD()),
                        new Command("warps", "WarpSystem", false, new WarpsCMD()),
                        new Command("delwarp", "WarpSystem", false, new DelWarpCMD()),
                        // WeatherSystem
                        new Command("day", "WeatherSystem", false, new DayCMD()),
                        new Command("night", "WeatherSystem", false, new NightCMD()),
                        new Command("sun", "WeatherSystem", false, new SunCMD()),
                        // Enderchest
                        new Command("enderchest", "Enderchest", false, new EnderchestCMD()),
                        new Command("ec", "Enderchest", false, new EnderchestCMD()),
                        new Command("openender", "Enderchest", false, new EnderchestCMD()),
                        // Speed
                        new Command("speed", "Speed", false, new SpeedCMD()),
                        // SpawnMob
                        new Command("spawnmob", "SpawnMob",  false, new SpawnMobCMD()),
                        // ItemSystem
                        new Command("give", "ItemSystem", false, new GiveCMD()),
                        new Command("item", "ItemSystem", false, new ItemCMD()),
                        new Command("i", "ItemSystem", false, new ItemCMD()),
                        // Inspector
                        new Command("inspector", "Inspector", false, new InspectorCMD()),
                        // WorldSystem
                        new Command("world", "WorldSystem", false, new WorldCMD()),
                        // not default since 2.0 DIV
                        new Command("msg", "MSG", false, new MessageCMD())
                )
        );

        // Register activated commands...
        for(Command command : commandRepository.getCommands().get(0)) {
            CommandAPI.registerCommand(command);
        }
        
        // Send command status to console at start xD
        CommandAPI.sendCommandStatusToConsole(commandRepository.getCommands());

        // NOTE : Join & Leave Messages -> Just say that its enabled
        
        //if (!new License(this.cfg.getString("LICENSE"), "http://skillmc.net/lizenzserver/verify.php", this).register()) return;

        Metrics metrics = new Metrics(this);

        Bukkit.getConsoleSender().sendMessage("");
        Bukkit.getConsoleSender().sendMessage("");
        Bukkit.getConsoleSender().sendMessage("§a§lEssentialsREL §7§l§7* §c§lsuccessfully connected to §e§lhttps§7§l://§e§lskillmc.net§7§l/§b§lservices§7§l/§c§l" + "essentialsreloaded" + "§7§l/");
        if (Main.getInstance().isMySQLEnabled()) {
            openMySQLConnection(fileRepository.getMysqlConfiguration().getString("mysql.host"), fileRepository.getMysqlConfiguration().getString("mysql.username"), fileRepository.getMysqlConfiguration().getString("mysql.password"), fileRepository.getMysqlConfiguration().getString("mysql.database"), fileRepository.getMysqlConfiguration().getInt("mysql.port"));
            SQLEventListener.createTable("coins");
        }
        Bukkit.getConsoleSender().sendMessage("");
        Bukkit.getConsoleSender().sendMessage("");
        Bukkit.getPluginManager().registerEvents(new EventListener(), this);
    }

    @Override
    public void onDisable() {
    }

    private void loadConfig() {

        // settings.yml
        fileRepository.getSettingsConfiguration().options().header("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■\n"
                + "■■                                                ■■\n■■     ESSENTIALSRELOADED | PunoxDEV | 2.0 DIV    ■■\n■■                                                ■■\n■■           SPIGOT VERSION: 1.8 - 1.13           ■■\n■■                                                ■■\n■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■"
                + "\n■■ Suggestions for improvement under:\n» Spigot: Punox\n» Skype: PunoxDEV\n■■ Thank's for downloading & using this plugin.\n"
                + "\n» Functions:\n● PaymentSystem\n● CoinsSystem\n● CoinsSettings\n● MSG\n● Gamemode\n● Broadcast\n● ChatClear\n"
                + "● Feed & Heal\n● Home & SetHome\n● KickSystem\n● Vanish\n● Invsee\n● TPA & TP System\n● ScammerSystem\n"
                + "● Warp & SetWarp System\n● Join & Leave Messages\n● WeatherSystem\n● Enderchest\n● Speed\n● SpawnMob\n● UpdateLog\n● Inspector\n● ItemSystem\n● WorldSystem\n■■ You can modify all relevant messages and settings as you like.\n");
        //this.cfg.addDefault("LICENSE", "YOUR LICENSE KEY HERE");
        fileRepository.getSettingsConfiguration().addDefault("settings", "");
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle", "");
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.PaymentSystem", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.WorldSystem", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.GameMode", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.Broadcast", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.ChatClear", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.Feed & Heal", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.Home & SetHome", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.KickSystem", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.Vanish", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.Invsee", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.Teleport", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.TPA", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.WeatherSystem", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.Enderchest", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.ScammerSystem", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.Speed", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.WarpSystem", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.Join & Leave Messages", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.SpawnMob", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.ItemSystem", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.Inspector", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.toggle.MSG", true);
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions", "");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.*", "essreloaded.*");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Enderchest", "essreloaded.enderchest");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Inspector", "");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Inspector.use", "essreloaded.inspector.use");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Inspector.--", "*");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Inspector.--", "*");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.ItemSystem", "");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.ItemSystem.give", "essreloaded.item.give");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.ItemSystem.item", "essreloaded.item.item");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.WorldSystem", "essreloaded.world");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.WeatherSystem", "");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.WeatherSystem.sun", "essreloaded.weather.sun");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.WeatherSystem.day", "essreloaded.weather.day");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.WeatherSystem.night", "essreloaded.weather.night");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.PaymentSystem", "");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.PaymentSystem.SeeCoins", "essreloaded.seecoins");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.PaymentSystem.PayCoins", "essreloaded.paycoins");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.PaymentSystem.CoinSettings", "essreloaded.admin");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.GameMode", "");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.GameMode.*", "essreloaded.gamemode.*");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.GameMode.Survival", "essreloaded.gamemode.0");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.GameMode.Creative", "essreloaded.gamemode.1");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.GameMode.Adventure", "essreloaded.gamemode.2");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.GameMode.Spectator", "essreloaded.gamemode.3");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Broadcast", "essreloaded.broadcast");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.ChatClear", "essreloaded.chatclear");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Invsee", "essreloaded.invsee");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Speed", "essreloaded.speed");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Feed", "essreloaded.feed");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Heal", "essreloaded.heal");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Fly.Self", "essreloaded.fly.self");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Fly.Other", "essreloaded.fly.other");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.HomeSystem", "");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.HomeSystem.Home", "essreloaded.home");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.HomeSystem.SetHome", "essreloaded.sethome");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Kick", "essreloaded.kick");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Vanish", "essreloaded.vanish");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.Teleport", "essreloaded.teleport");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.TPA", "essreloaded.tpa");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.ReportScammer", "essreloaded.scammer.report");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.WarpSystem", "");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.WarpSystem.Warp", "essreloaded.warpsystem.warp");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.WarpSystem.SetWarp", "essreloaded.warpsystem.setwarp");
        fileRepository.getSettingsConfiguration().addDefault("settings.permissions.MSG", "essreloaded.msg");
        fileRepository.getSettingsConfiguration().addDefault("settings.setup.PaymentSystem", "");
        fileRepository.getSettingsConfiguration().addDefault("settings.setup.PaymentSystem.StartCoins", Integer.valueOf(1000));
        fileRepository.getSettingsConfiguration().addDefault("settings.setup.ScammerSystem", "");
        fileRepository.getSettingsConfiguration().addDefault("settings.setup.ScammerSystem.KickAfterReports", Integer.valueOf(5));
        fileRepository.getSettingsConfiguration().addDefault("settings.setup.ScammerSystem.BanTimeInMinutes", Integer.valueOf(10));
        // coins.yml

        fileRepository.getCoinsConfiguration().addDefault("coins", "");

        // worlds.yml

        fileRepository.getWorldsConfiguration().addDefault("worlds", "");

        // mysql.yml

        fileRepository.getMysqlConfiguration().options().header("■■ If you activate MySQL, all coins will be saved to your database. Local saving of data is no longer active. \n");

        fileRepository.getMysqlConfiguration().addDefault("mysql.enable", false);
        fileRepository.getMysqlConfiguration().addDefault("mysql.host", "");
        fileRepository.getMysqlConfiguration().addDefault("mysql.username", "");
        fileRepository.getMysqlConfiguration().addDefault("mysql.password", "");
        fileRepository.getMysqlConfiguration().addDefault("mysql.database", "");
        fileRepository.getMysqlConfiguration().addDefault("mysql.port", 3306);

        // messages.yml

        fileRepository.getMessagesConfiguration().options().header("# messages.yml");
        fileRepository.getMessagesConfiguration().addDefault("messages", "");
        fileRepository.getMessagesConfiguration().addDefault("messages.Prefix", "&a&lEssentialsREL &7&l&7» ");
        fileRepository.getMessagesConfiguration().addDefault("messages.NoPermission", "&7&lYou have no rights to execute this command...");
        fileRepository.getMessagesConfiguration().addDefault("messages.JoinListener", "");
        fileRepository.getMessagesConfiguration().addDefault("messages.JoinListener.JoinMessage", "&7» &a%player% &ejoined the game...");
        fileRepository.getMessagesConfiguration().addDefault("messages.JoinListener.LeaveMessage", "&7» &c%player% &eleft the game...");
        fileRepository.getMessagesConfiguration().addDefault("messages.Help", "");
        fileRepository.getMessagesConfiguration().addDefault("messages.Help.1", "&e&lCommands & Informations");
        fileRepository.getMessagesConfiguration().addDefault("messages.Help.2", "&e&lCommands&7&l: &r&7/coinssettings, /coins, /world, /gm, /gamemode, /sethome, /home, /pay, /warp, /setwarp, /msg, /kick, /feed, /heal, /fly, /broadcast, /warps, /chatclear, /cc, /tpaccept, /tpadeny, /tpa, /tp, /tphere, /enderchest, /ec, /sun, /day, /night, /invsee, /vanish, /v, /money, /dollar, /geld, /help, /warps, /spawnmob, /updatelog, /tpo, /item, /i, /inspector, /world, /delwarp, /delhome, /homes");
        fileRepository.getMessagesConfiguration().addDefault("messages.Usage", "&e&lUsage&7&l:");
        fileRepository.getMessagesConfiguration().addDefault("messages.Coins", "&7&lYour current balance&7&l: &6&l%balance% Coins");
        fileRepository.getMessagesConfiguration().addDefault("messages.FeedAndHeal", "");
        fileRepository.getMessagesConfiguration().addDefault("messages.FeedAndHeal.Success", "&7&lYou healed yourself successfully...");
        fileRepository.getMessagesConfiguration().addDefault("messages.Kick", "");
        fileRepository.getMessagesConfiguration().addDefault("messages.Kick.1", "&e&lYou have been kicked from the server&7&l...");
        fileRepository.getMessagesConfiguration().addDefault("messages.Kick.2", "&c&lReason&7&l: %reason%");
        fileRepository.getMessagesConfiguration().addDefault("messages.Kick.3", "&e&lsystem coded by PunoxDEV // edit in settings.yml");
        fileRepository.getMessagesConfiguration().addDefault("messages.MSG", "");
        fileRepository.getMessagesConfiguration().addDefault("messages.MSG.ToMe", "&e&l%player% &7&l» &e&lME &7&l● &e&l");
        fileRepository.getMessagesConfiguration().addDefault("messages.MSG.ToOther", "&e&lME &7&l» &e&l%player% &7&l● &e&l");
        fileRepository.getMessagesConfiguration().addDefault("messages.ChatClear", "&7&lThe Chat has been cleared by &e&l%player%");

        fileRepository.getSettingsConfiguration().options().copyDefaults(true);
        fileRepository.getCoinsConfiguration().options().copyDefaults(true);
        fileRepository.getWarpsConfiguration().options().copyDefaults(true);
        fileRepository.getWorldsConfiguration().options().copyDefaults(true);
        fileRepository.getMysqlConfiguration().options().copyDefaults(true);
        fileRepository.getMessagesConfiguration().options().copyDefaults(true);

        try {
            fileRepository.getSettingsConfiguration().save(fileRepository.getSettingsFile());
            fileRepository.getCoinsConfiguration().save(fileRepository.getCoinsFile());
            fileRepository.getWarpsConfiguration().save(fileRepository.getWarpsFile());
            fileRepository.getWorldsConfiguration().save(fileRepository.getWorldsFile());
            fileRepository.getMysqlConfiguration().save(fileRepository.getMysqlFile());
            fileRepository.getMessagesConfiguration().save(fileRepository.getMessagesFile());

            for (World w : Bukkit.getServer().getWorlds()) {
                for (int i = 0; i < fileRepository.getWorldsConfiguration().getKeys(false).size(); i++) {
                    if (fileRepository.getWorldsConfiguration().getString(i + "." + w.getName()) == null) {
                        fileRepository.getWorldsConfiguration().addDefault("worlds." + "." + w.getName() + ".pvp", true);
                        fileRepository.getWorldsConfiguration().addDefault("worlds." + "." + w.getName() + ".animals", true);
                        fileRepository.getWorldsConfiguration().addDefault("worlds." + "." + w.getName() + ".monsters", true);
                        fileRepository.getWorldsConfiguration().save(fileRepository.getWorldsFile());
                    }

                }
            }
        } catch (Exception localException) {
        }
    }

    private void openMySQLConnection(String host, String user, String password, String database, int port) {
        this.mySQL = new MySQL(host, user, password, database, port);
    }
}
