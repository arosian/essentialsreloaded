package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetWarpCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.WarpSystem.SetWarp");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission))) {
				if (args.length == 0) {
					p.sendMessage(this.prefix + usage + " §7§l/setwarp [Name]");
				} else if (args.length == 1) {
					try {
						main.getFileRepository().getWarpsConfiguration().load(main.getFileRepository().getWarpsFile());
					} catch (Exception localException) {
					}
					//int id = this.cfg2.getKeys(false).size();
					if (!main.getFileRepository().getWarpsConfiguration().contains(args[0].toLowerCase() + ".world")) {

						String world = p.getLocation().getWorld().getName();
						double x = p.getLocation().getX();
						double y = p.getLocation().getY();
						double z = p.getLocation().getZ();
						double pitch = p.getLocation().getPitch();
						double yaw = p.getLocation().getYaw();

						//this.cfg2.set(id + "", args[0].toLowerCase());
						main.getFileRepository().getWarpsConfiguration().set(args[0].toLowerCase() + ".world", world);
						main.getFileRepository().getWarpsConfiguration().set(args[0].toLowerCase() + ".x", Double.valueOf(x));
						main.getFileRepository().getWarpsConfiguration().set(args[0].toLowerCase() + ".y", Double.valueOf(y));
						main.getFileRepository().getWarpsConfiguration().set(args[0].toLowerCase() + ".z", Double.valueOf(z));
						main.getFileRepository().getWarpsConfiguration().set(args[0].toLowerCase() + ".pitch", Double.valueOf(pitch));
						main.getFileRepository().getWarpsConfiguration().set(args[0].toLowerCase() + ".yaw", Double.valueOf(yaw));
						
						p.sendMessage(this.prefix + "§7§lYou §a§lsuccessfully §7§lcreated the warp §e§l" + args[0].toLowerCase());
						try {
							main.getFileRepository().getWarpsConfiguration().save(main.getFileRepository().getWarpsFile());
						} catch (Exception localException1) {
						}
					} else {
						p.sendMessage(this.prefix + "§7§lThe warp §6§l" + args[0] + " §7§lalready exists.");
					}
				} else {
					p.sendMessage(this.prefix + usage + " §7§l/setwarp [Name]");
				}
			} else {
				p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
			}
		} else {
			cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
		}
		return false;
	}
}
