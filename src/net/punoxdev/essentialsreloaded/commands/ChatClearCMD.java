package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChatClearCMD implements CommandExecutor {

	private Main main = Main.getInstance();
	
	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.ChatClear");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if (p.hasPermission(permission) || p.hasPermission(adminpermission)) {
				if(args.length == 0) {
					for (Player all : Bukkit.getOnlinePlayers()) {
						for (int i = 0; i < 100; i++) {
							all.sendMessage("");
						}
						all.sendMessage(this.prefix + MessageAPI.getMessageFromConfig(fileRepository.getMessagesConfiguration(), "messages.ChatClear").replaceAll("%player%", p.getName()));
					}
				} else {
					p.sendMessage(this.prefix + usage + " �7�l/cc");
				}
			} else {
				p.sendMessage(this.prefix + MessageAPI.getMessageFromConfig(fileRepository.getMessagesConfiguration(), "messages.NoPermission"));
			}

		} else {
			cs.sendMessage(prefix + "�e�lYou have to be a player to perform this action.");
		}

		return false;
	}
}