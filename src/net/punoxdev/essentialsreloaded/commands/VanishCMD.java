package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class VanishCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.Vanish");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	public static ArrayList<Player> hide = new ArrayList();

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if (p.hasPermission(permission)) {
				if (args.length == 0) {
					if (!hide.contains(p)) {
						hide.add(p);
						for (Player all : Bukkit.getOnlinePlayers()) {
							all.hidePlayer(p);
						}
						p.sendMessage(this.prefix + "§7§lYou are now §e§lvanished");
					} else {
						hide.remove(p);
						for (Player all : Bukkit.getOnlinePlayers()) {
							all.showPlayer(p);
							all.canSee(p);
						}
						p.sendMessage(this.prefix + "§7§lYou are now §e§lvisible");

					}
				} else if (args.length == 1) {
					Player t = Bukkit.getPlayer(args[0]);
					try {
						if (!hide.contains(p)) {
							for (Player all : Bukkit.getOnlinePlayers()) {
								all.hidePlayer(p);
							}
							t.sendMessage(this.prefix + "§7§lYou are now vanished");
							p.sendMessage(this.prefix + "§7§lThe player §e§l" + t.getName() + " §7§lis now §e§lvanished");
						} else {
							for (Player all : Bukkit.getOnlinePlayers()) {
								all.showPlayer(p);
								all.canSee(p);
							}
							t.sendMessage(this.prefix + "§7§lYou are now visible");
							p.sendMessage(this.prefix + "§7§lThe player §e§l" + t.getName() + " §7§lis now §e§lvisible");
						}
					} catch (Exception e) {
						p.sendMessage(this.prefix + "§7§lThat player isn't online...");
					}
				} else {
					p.sendMessage(this.prefix + usage + " §7§l/vanish <Player>");
				}
			} else {
				p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
			}
		} else {
			cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
		}

		return false;
	}
}
