package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.CoinsAPI;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PayCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.PaymentSystem.PayCoins");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission))) {
				try {
					main.getFileRepository().getCoinsConfiguration().load(main.getFileRepository().getCoinsFile());
				} catch (Exception localException1) {
				}

				if (args.length == 0) {
					p.sendMessage(this.prefix + usage + " §7§l/pay [Player] [Integer]");
				} else if (args.length == 1) {
					p.sendMessage(this.prefix + usage + " §7§l/pay [Player] [Integer]");
				} else if (args.length == 2) {
					Player t = Bukkit.getPlayer(args[0]);
					String anzahl = args[1];
					int betrag = Integer.parseInt(anzahl);

					try {
						if (p != t) {
						if (CoinsAPI.getCoins(p.getName()) >= betrag) {
							if(betrag > 0) {
								CoinsAPI.addCoins(t.getName(), betrag);
								CoinsAPI.removeCoins(p.getName(), betrag);
								p.sendMessage(this.prefix + "§7§lYou successfully gave §6§l" + betrag + " Coins §7§lto §e§l" + t.getName() + "");
								t.sendMessage(this.prefix + "§7§lYou successfully got §6§l" + betrag + " Coins §7§lfrom §e§l" + p.getName() + "");
								
							} else {
								p.sendMessage(this.prefix + "§7§lYou can only pay with positive amounts");
							}
						  } else {
							p.sendMessage(this.prefix + "§7§lYou do not have enough money");
						  }
						} else {
							p.sendMessage(this.prefix + "§7§lYou can't pay to yourself");
						}
					} catch (Exception e) {
						p.sendMessage(this.prefix + "§7§lThat player isn't online...");
					}
				} else {
					p.sendMessage(this.prefix + usage + " §7§l/pay <Player> <Integer>");
				}
			} else {
				p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
			}
		} else {
			cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
		}

		return false;
	}
}
