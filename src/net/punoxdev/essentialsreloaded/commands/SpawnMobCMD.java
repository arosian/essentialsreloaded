package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.Set;

public class SpawnMobCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.WarpSystem.SetWarp");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission))) {
				if (args.length == 2) {
					final String mob = args[0];
					final String mobcount = args[1];
					int spawnrate = Integer.parseInt(mobcount);
					try {
						if (mob.contains("ZOMBIE") | mob.contains("Zombie") | mob.contains("zombie")) {
							p.sendMessage(this.prefix + "§c§oSummoned§7: §e§l§o" + mobcount
									+ " §e§l§oEntitys §8§l| §8§o(§e§lTYPE§8§l: §1§l§oZOMBIE§8§o)");
							Block targetblock = p.getTargetBlock((Set<Material>) null, 100);
							targetblock.getLocation().setY(targetblock.getY() + 2);
							Location loc = targetblock.getLocation();
							for (int spawn = 0; spawn < spawnrate; spawn++) {
								p.getWorld().spawnEntity(loc, EntityType.ZOMBIE);
							}
						}

						if (mob.contains("ENDERDRAGON") || mob.contains("Enderdragon") || mob.contains("enderdragon")
								|| mob.contains("EnderDragon")) {
							p.sendMessage(this.prefix + "§c§oSummoned§7: §e§l§o" + mobcount
									+ " §e§l§oEntitys §8§l| §8§o(§e§lTYPE§8§l: §1§l§oENDERDRAGON§8§o)");
							Block targetblock = p.getTargetBlock((Set<Material>) null, 100);
							targetblock.getLocation().setY(targetblock.getY() + 2);
							Location loc = targetblock.getLocation();
							for (int spawn = 0; spawn < spawnrate; spawn++) {
								p.getWorld().spawnEntity(loc, EntityType.ENDER_DRAGON);
							}
						}

						if (mob.contains("PIG") || mob.contains("Pig") || mob.contains("pig")) {
							p.sendMessage(this.prefix + "§c§oSummoned§7: §e§l§o" + mobcount
									+ " §e§l§oEntitys §8§l| §8§o(§e§lTYPE§8§l: §1§l§oPIG§8§o)");
							Block targetblock = p.getTargetBlock((Set<Material>) null, 100);
							targetblock.getLocation().setY(targetblock.getY() + 2);
							Location loc = targetblock.getLocation();
							for (int spawn = 0; spawn < spawnrate; spawn++) {
								p.getWorld().spawnEntity(loc, EntityType.PIG);
							}
						}

						if (mob.contains("SHEEP") || mob.contains("Sheep") || mob.contains("sheep")) {
							p.sendMessage(this.prefix + "§c§oSummoned§7: §e§l§o" + mobcount
									+ " §e§l§oEntitys §8§l| §8§o(§e§lTYPE§8§l: §1§l§oSHEEP§8§o)");
							Block targetblock = p.getTargetBlock((Set<Material>) null, 100);
							targetblock.getLocation().setY(targetblock.getY() + 2);
							Location loc = targetblock.getLocation();
							for (int spawn = 0; spawn < spawnrate; spawn++) {
								p.getWorld().spawnEntity(loc, EntityType.SHEEP);
							}
						}

						if (mob.contains("CREEPER") || mob.contains("Creeper") || mob.contains("creeper")) {
							p.sendMessage(this.prefix + "§c§oSummoned§7: §e§l§o" + mobcount
									+ " §e§l§oEntitys §8§l| §8§o(§e§lTYPE§8§l: §1§l§oCREEPER§8§o)");
							Block targetblock = p.getTargetBlock((Set<Material>) null, 100);
							targetblock.getLocation().setY(targetblock.getY() + 2);
							Location loc = targetblock.getLocation();
							for (int spawn = 0; spawn < spawnrate; spawn++) {
								p.getWorld().spawnEntity(loc, EntityType.CREEPER);
							}
						}

						if (mob.contains("BLAZE") || mob.contains("Blaze") || mob.contains("blaze")) {
							p.sendMessage(this.prefix + "§c§oSummoned§7: §e§l§o" + mobcount
									+ " §e§l§oEntitys §8§l| §8§o(§e§lTYPE§8§l: §1§l§oBLAZE§8§o)");
							Block targetblock = p.getTargetBlock((Set<Material>) null, 100);
							targetblock.getLocation().setY(targetblock.getY() + 2);
							Location loc = targetblock.getLocation();
							for (int spawn = 0; spawn < spawnrate; spawn++) {
								p.getWorld().spawnEntity(loc, EntityType.BLAZE);
							}
						}

						if (mob.contains("COW") || mob.contains("Cow") || mob.contains("cow")) {
							p.sendMessage(this.prefix + "§c§oSummoned§7: §e§l§o" + mobcount
									+ " §e§l§oEntitys §8§l| §8§o(§e§lTYPE§8§l: §1§l§oCOW§8§o)");
							Block targetblock = p.getTargetBlock((Set<Material>) null, 100);
							targetblock.getLocation().setY(targetblock.getY() + 2);
							Location loc = targetblock.getLocation();
							for (int spawn = 0; spawn < spawnrate; spawn++) {
								p.getWorld().spawnEntity(loc, EntityType.COW);
							}
						}

						if (mob.contains("TNT_MINECART") || mob.contains("TnT_Minecart") || mob.contains("tnt_minecart")
								|| mob.contains("TnT_minecart")) {
							p.sendMessage(this.prefix + "§c§oSummoned§7: §e§l§o" + mobcount
									+ " §e§l§oEntitys §8§l| §8§o(§e§lTYPE§8§l: §1§l§oTNT_MINECART§8§o)");
							Block targetblock = p.getTargetBlock((Set<Material>) null, 100);
							targetblock.getLocation().setY(targetblock.getY() + 2);
							Location loc = targetblock.getLocation();
							for (int spawn = 0; spawn < spawnrate; spawn++) {
								p.getWorld().spawnEntity(loc, EntityType.MINECART_TNT);
							}
						}

						if (mob.contains("SILVERFISH") || mob.contains("Silverfish") || mob.contains("silverfish")
								|| mob.contains("SilverFish")) {
							p.sendMessage(this.prefix + "§c§oSummoned§7: §e§l§o" + mobcount
									+ " §e§l§oEntitys §8§l| §8§o(§e§lTYPE§8§l: §1§l§oSILVERFISH§8§o)");
							Block targetblock = p.getTargetBlock((Set<Material>) null, 100);
							targetblock.getLocation().setY(targetblock.getY() + 2);
							Location loc = targetblock.getLocation();
							for (int spawn = 0; spawn < spawnrate; spawn++) {
								p.getWorld().spawnEntity(loc, EntityType.SILVERFISH);
							}
						}

						if (mob.contains("GUARDIAN") || mob.contains("Guardian") || mob.contains("guardian")) {
							p.sendMessage(this.prefix + "§c§oSummoned§7: §e§l§o" + mobcount
									+ " §e§l§oEntitys §8§l| §8§o(§e§lTYPE§8§l: §1§l§oGUARDIAN§8§o)");
							Block targetblock = p.getTargetBlock((Set<Material>) null, 100);
							targetblock.getLocation().setY(targetblock.getY() + 2);
							Location loc = targetblock.getLocation();
							for (int spawn = 0; spawn < spawnrate; spawn++) {
								p.getWorld().spawnEntity(loc, EntityType.GUARDIAN);
							}
						}

						if (mob.contains("CHICKEN") || mob.contains("Chicken") || mob.contains("chicken")) {
							p.sendMessage(this.prefix + "§c§oSummoned§7: §e§l§o" + mobcount
									+ " §e§l§oEntitys §8§l| §8§o(§e§lTYPE§8§l: §1§l§oCHICKEN§8§o)");
							Block targetblock = p.getTargetBlock((Set<Material>) null, 100);
							targetblock.getLocation().setY(targetblock.getY() + 2);
							Location loc = targetblock.getLocation();
							for (int spawn = 0; spawn < spawnrate; spawn++) {
								p.getWorld().spawnEntity(loc, EntityType.CHICKEN);
							}
						}
					} catch (NullPointerException e) {
						p.sendMessage("§8§m------------------[§r §4§lMob Types §8§m]------------------");
						p.sendMessage("§8§l» §e§lZOMBIE §8(§4§oEvil§8)");
						p.sendMessage("§8§l» §e§lENDERDRAGON §8(§4§oBoss§8)");
						p.sendMessage("§8§l» §e§lBLAZE §8(§4§oEvil§8)");
						p.sendMessage("§8§l» §e§lSILVERFISH §8(§4§oEvil§8)");
						p.sendMessage("§8§l» §e§lCREEPER §8(§4§oEvil§8)");
						p.sendMessage("§8§l» §e§lGUARDIAN §8(§4§oEvil§8)");
						p.sendMessage("§8§l» §e§lSHEEP §8(§d§oAnimal§8)");
						p.sendMessage("§8§l» §e§lPIG §8(§d§oAnimal§8)");
						p.sendMessage("§8§l» §e§lCOW §8(§d§oAnimal§8)");
						p.sendMessage("§8§l» §e§lCHICKEN §8(§d§oAnimal§8)");
						p.sendMessage("§8§l» §e§lTNT_MINECART §8(§d§bEntity§8)");
						p.sendMessage("§8§m------------------------------------------------");
						p.sendMessage(
								this.prefix + usage + " §7§l/spawnmob [EntityType] [Integer]§7.");
					}
				} else {
					p.sendMessage("§8§m------------------[§r §4§lMob Types §8§m]------------------");
					p.sendMessage("§8§l» §e§lZOMBIE §8(§4§oEvil§8)");
					p.sendMessage("§8§l» §e§lENDERDRAGON §8(§4§oBoss§8)");
					p.sendMessage("§8§l» §e§lBLAZE §8(§4§oEvil§8)");
					p.sendMessage("§8§l» §e§lSILVERFISH §8(§4§oEvil§8)");
					p.sendMessage("§8§l» §e§lCREEPER §8(§4§oEvil§8)");
					p.sendMessage("§8§l» §e§lGUARDIAN §8(§4§oEvil§8)");
					p.sendMessage("§8§l» §e§lSHEEP §8(§d§oAnimal§8)");
					p.sendMessage("§8§l» §e§lPIG §8(§d§oAnimal§8)");
					p.sendMessage("§8§l» §e§lCOW §8(§d§oAnimal§8)");
					p.sendMessage("§8§l» §e§lCHICKEN §8(§d§oAnimal§8)");
					p.sendMessage("§8§l» §e§lTNT_MINECART §8(§d§bEntity§8)");
					p.sendMessage("§8§m------------------------------------------------");
					p.sendMessage(
							this.prefix + usage + " §7§l/spawnmob [EntityType] [Integer]§7.");
				}
			} else {
				p.sendMessage(this.prefix +main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
			}
		}
		return false;
	}
}
