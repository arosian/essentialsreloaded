package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KickCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.Kick");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	private String kickline1 = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Kick.1");
	private String kickline2 = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Kick.2");
	private String kickline3 = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Kick.3");

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission))) {
				if (args.length == 0) {
					p.sendMessage(this.prefix + usage + " §7§l/kick [Player] [Reason]");
				} else if (args.length == 1) {
					p.sendMessage(this.prefix + usage + " §7§l/kick [Player] [Reason]");
				} else if (args.length == 2) {
					Player t = Bukkit.getPlayer(args[0]);
					try {
						String grund = args[1];
						t.kickPlayer(
								prefix + kickline1 + "\n" + kickline2.replaceAll("%reason%", grund) + "\n" + kickline3);

					} catch (Exception e) {
						p.sendMessage(this.prefix + "§7§lThat player isn't online...");
					}
				} else {
					p.sendMessage(this.prefix + usage + " §7§l/kick [Player] [Reason]");
				}
			} else {
				p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
			}
		} else {
			cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
		}

		return false;
	}
}