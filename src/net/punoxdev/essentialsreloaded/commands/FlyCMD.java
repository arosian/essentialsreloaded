package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class FlyCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");

	private String permissionFlySelf = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.Fly.Self");
	private String permissionFlyOther = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.Fly.Other");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");
		   
	public static HashMap<Player, Boolean> checkfly = new HashMap<>();

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player)cs;
		if ((cs instanceof Player)) {
			if ((p.hasPermission(permissionFlySelf)) || (p.hasPermission(adminpermission))) {
				if (args.length == 0) {
					if (!p.getAllowFlight()) {
						p.sendMessage("");
						p.sendMessage("§8§m------------------[§r §6§lPlayerSystem §8§m]------------------");
						p.sendMessage("§8§l» §7§lYour §e§lfly §7§lhas been §a§lenabled§7§l.");
						p.sendMessage("§8§m---------------------------------------------------");
						p.sendMessage("");
						p.setAllowFlight(true);
					} else {
						p.sendMessage("");
						p.sendMessage("§8§m------------------[§r §6§lPlayerSystem §8§m]------------------");
						p.sendMessage("§8§l» §7§lYour §e§lfly §7§lhas been §c§ldisabled§7§l.");
						p.sendMessage("§8§m---------------------------------------------------");
						p.sendMessage("");
						p.setAllowFlight(false);
					}
				} else if (args.length == 1) {
					Player t = Bukkit.getPlayer(args[0]);
					if ((p.hasPermission(permissionFlyOther)) || (p.hasPermission(adminpermission))) {
						if(Bukkit.getOnlinePlayers().contains(t)){
							if(t.getAllowFlight()){
								t.setAllowFlight(false);	
								t.setFlying(false);
								t.sendMessage("§8§m------------------[§r §6§lPlayerSystem §8§m]------------------");
								t.sendMessage("§8§l» §7§lYour §e§lfly §7§lhas been §c§ldisabled§7§l.");
								t.sendMessage("§8§m------------------------------------------------");
								t.sendMessage("");
								
								p.sendMessage("§8§m------------------[§r §6§lPlayerSystem §8§m]------------------");
								p.sendMessage("§8§l» §7§lThe §e§lfly mode §7§lof §e§l" + t.getName() + " §7§lhas been §c§ldisabled§7§l.");
								p.sendMessage("§8§m---------------------------------------------------");	
								p.sendMessage("");
					
							} else if(!t.getAllowFlight()) {
								t.setAllowFlight(true);	
								t.setFlying(true);
								t.sendMessage("§8§m------------------[§r §6§lPlayerSystem §8§m]------------------");
								t.sendMessage("§8§l» §7§lYour §e§lfly §7§lhas been §a§lenabled§7§l.");
								t.sendMessage("§8§m---------------------------------------------------");
								t.sendMessage("");
									
									
								p.sendMessage("§8§m------------------[§r §6§lPlayerSystem §8§m]------------------");
								p.sendMessage("§8§l» §7§lThe §e§lfly mode §7§lof §e§l" + t.getName() + " §7§lhas been §a§lenabled§7§l.");
								p.sendMessage("§8§m---------------------------------------------------");
								p.sendMessage("");
							}
						}else{
							p.sendMessage(this.prefix + "§7§lThat player isn't online...");
						}
			 		}else {
						p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
			 			}
					}
				}else{
					p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
					
				}
			}
		else {cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
	}
		return false;
		}
	}
