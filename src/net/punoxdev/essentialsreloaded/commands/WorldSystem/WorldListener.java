package net.punoxdev.essentialsreloaded.commands.WorldSystem;

import org.bukkit.entity.Animals;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntitySpawnEvent;

import net.punoxdev.essentialsreloaded.Main;

public class WorldListener implements Listener{

	private Main main = Main.getInstance();
	
	@EventHandler
	public void onMobSpawn(EntitySpawnEvent e) {
		try {
			main.getFileRepository().getWorldsConfiguration().load(main.getFileRepository().getWorldsFile());
			}
		catch(Exception exception) {
		}
		String worldToCheck = e.getEntity().getWorld().getName();
		if(e.getEntity() instanceof Animals) {
			if(!main.getFileRepository().getWorldsConfiguration().getBoolean("worlds." + "." + worldToCheck + ".animals")) {
				e.setCancelled(true);
			}
		}
		
		if(e.getEntity() instanceof Monster) {
			if(!main.getFileRepository().getWorldsConfiguration().getBoolean("worlds." + "." + worldToCheck + ".monsters")) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlayerDamage(EntityDamageByEntityEvent e) {
		try {main.getFileRepository().getWorldsConfiguration().load(main.getFileRepository().getWorldsFile());
			}catch(Exception exception) {}
		String worldToCheck = e.getEntity().getWorld().getName();
		if(e.getEntity() instanceof Player) {
			if(!main.getFileRepository().getWorldsConfiguration().getBoolean("worlds." + "." + worldToCheck + ".pvp"));
				e.setCancelled(true);
			
		}
		
	}

}
