package net.punoxdev.essentialsreloaded.commands.WorldSystem;


import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;

public class WorldCMD implements CommandExecutor {

    private Main main = Main.getInstance();

    private FileRepository fileRepository = main.getFileRepository();

    private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
    private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

    private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.WorldSystem");
    private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        Player p = (Player) cs;
        if ((cs instanceof Player)) {
            if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission))) {
                try {
                    try {
                        main.getFileRepository().getWorldsConfiguration().load(main.getFileRepository().getWorldsFile());
                    } catch (Exception e) {
                    }
                    if (args.length == 2) {
                        if (args[0].equals("create")) {
                            String nameOfMapToCreate = args[1];
                            if (Bukkit.getWorld(nameOfMapToCreate) == null) {
                                p.sendMessage(this.prefix.replaceAll("&", "§") + "§7§lYou successfully created the\n §e§lworld §7§lnamed §e§l" + nameOfMapToCreate);
                                World world = Bukkit.createWorld(new WorldCreator(nameOfMapToCreate));
                            } else {
                                p.sendMessage(this.prefix.replaceAll("&", "§") + "§7§lThe §e§lworld §7§lnamed §e§l" + nameOfMapToCreate + " §7§lalready exists...");
                            }
                        }

                        if (args[0].equals("teleport") || args[0].equals("tp")) {
                            p.teleport(Bukkit.getWorld(args[1]).getSpawnLocation());
                            p.sendMessage(this.prefix.replaceAll("&", "§") + "§7§lYou have been teleported to\n the §e§lworld §7§lnamed §e§l" + args[1] + "§7§l...");
                        } else {
                            p.sendMessage(this.prefix.replaceAll("&", "§") + "§7§lThe §e§lworld §7§lnamed §e§l" + args[1] + " §7§ldoesnt exist...");
                        }
                    } else if (args.length == 4) {
                        if (args[0].equals("modify")) {
                            if (Bukkit.getWorld(args[1]) != null) {
                                String worldToModify = args[1];
                                if (args[2].equals("pvp")) {
                                    if (args[3].equals("true")) {
                                        Bukkit.getWorld(worldToModify).setPVP(true);
                                        p.sendMessage(this.prefix.replaceAll("&", "§") + "§7§lYou have successfully §e§lenabled §8(§c§oPVP§8)\n §7§lin the world named §e§l" + args[1] + "§7§l...");
                                        main.getFileRepository().getWorldsConfiguration().set("worlds." + "." + worldToModify + ".pvp", true);
                                    } else {
                                        Bukkit.getWorld(worldToModify).setPVP(false);
                                        p.sendMessage(this.prefix.replaceAll("&", "§") + "§7§lYou have successfully §e§ldisabled §8(§c§oPVP§8)\n §7§lin the world named §e§l" + args[1] + "§7§l...");
                                        main.getFileRepository().getWorldsConfiguration().set("worlds." + "." + worldToModify + ".pvp", false);
                                    }

                                } else if (args[2].equals("animals")) {
                                    if (args[3].equals("true")) {
                                        Bukkit.getWorld(worldToModify).setAnimalSpawnLimit(10);
                                        p.sendMessage(this.prefix.replaceAll("&", "§") + "§7§lYou have successfully §e§lenabled §8(§c§oANIMALS§8)\n §7§lin the world named §e§l" + args[1] + "§7§l...");
                                        main.getFileRepository().getWorldsConfiguration().set("worlds." + "." + worldToModify + ".animals", true);
                                    } else {
                                        for (Entity e : Bukkit.getWorld(worldToModify).getEntities()) {
                                            if (e instanceof Animals) {
                                                e.remove();
                                            }
                                            Bukkit.getWorld(worldToModify).setAnimalSpawnLimit(0);
                                        }
                                        p.sendMessage(this.prefix.replaceAll("&", "§") + "§7§lYou have successfully §e§ldisabled §8(§c§oANIMALS§8)\n §7§lin the world named §e§l" + args[1] + "§7§l...");
                                        main.getFileRepository().getWorldsConfiguration().set("worlds." + "." + worldToModify + ".animals", false);
                                    }

                                } else if (args[2].equals("monsters")) {
                                    if (args[3].equals("true")) {
                                        Bukkit.getWorld(worldToModify).setMonsterSpawnLimit(10);
                                        p.sendMessage(this.prefix.replaceAll("&", "§") + "§7§lYou have successfully §e§lenabled §8(§c§oMONSTERS§8)\n §7§lin the world named §e§l" + args[1] + "§7§l...");
                                        main.getFileRepository().getWorldsConfiguration().set("worlds." + "." + worldToModify + ".monsters", true);
                                    } else {
                                        for (Entity e : Bukkit.getWorld(worldToModify).getEntities()) {
                                            if (e instanceof Monster) {
                                                e.remove();
                                            }
                                            Bukkit.getWorld(worldToModify).setMonsterSpawnLimit(0);
                                        }
                                        p.sendMessage(this.prefix.replaceAll("&", "§") + "§7§lYou have successfully §e§ldisabled §8(§c§oMONSTERS§8)\n §7§lin the world named §e§l" + args[1] + "§7§l...");
                                        main.getFileRepository().getWorldsConfiguration().set("worlds." + "." + worldToModify + ".monsters", false);
                                    }
                                }

                            } else {
                                p.sendMessage(this.prefix.replaceAll("&", "§") + "§7§lThe §e§lworld §7§lnamed §e§l" + args[1] + " §7§ldoesnt exist...");
                            }
                        }

                    } else {
                        throw new NullPointerException();
                    }

                } catch (NullPointerException e) {

                    p.sendMessage("§8§m------------------[§r §4§lWORLDSYSTEM §8§m]------------------");
                    p.sendMessage("");
                    p.sendMessage("§8§l» §c§lAVAILABLE COMMANDS§7:");
                    p.sendMessage("");
                    p.sendMessage("  §8§l» §e§l/world create §8(§c§oName§8)");
                    p.sendMessage("  §8§l» §e§l/world teleport §8(§c§oName§8)");
                    p.sendMessage("  §8§l» §e§l/world modify §8(§c§oName§8) §8(§c§oSetting§8) §8(§c§oTrue / False§8)");
                    p.sendMessage("");
                    p.sendMessage("§8(§c§oSettings: §7§opvp, animals, monsters§8)");
                    p.sendMessage("§8§m------------------------------------------------");
                    p.sendMessage("");
                    p.sendMessage(this.prefix.replaceAll("&", "§") + usage + " §7§l/world [Command]§7.");

                }
                try {
                    main.getFileRepository().getWorldsConfiguration().save(main.getFileRepository().getWorldsFile());
                } catch (Exception e) {
                    //
                }
            } else {
                p.sendMessage(this.prefix.replaceAll("&", "§") + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
            }
        } else {
            p.sendMessage(this.prefix.replaceAll("&", "§") + "§e§lYou have to be a player to perform this action.");
        }

        return false;

    }


}
