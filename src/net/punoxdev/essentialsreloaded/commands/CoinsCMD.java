package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.CoinsAPIFile;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CoinsCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.PaymentSystem.SeeCoins");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	private String coinsmessage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Coins");

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission))) {
				try {
					main.getFileRepository().getCoinsConfiguration().load(main.getFileRepository().getCoinsFile());
				} catch (Exception localException) {
				}

				if (args.length == 0) {
					if (CoinsAPIFile.fileExist()) {
						if (main.getFileRepository().getCoinsConfiguration().contains("coins." + p.getName())) {
							try {
								main.getFileRepository().getCoinsConfiguration().load(main.getFileRepository().getCoinsFile());
							} catch (Exception localException1) {
							}

							p.sendMessage(this.prefix
									+ coinsmessage.replace("%balance%", CoinsAPIFile.getCoinsString(p)));
						} else {
							p.sendMessage(
									this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
						}
					} else {
						p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
					}
				} else {
					p.sendMessage(this.prefix + usage + " §7§l/coins");
				}
			} else {
				p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
			}
		} else {
			cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
		}

		return false;
	}
}
