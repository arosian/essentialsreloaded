package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UpdateLogCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.WarpSystem.SetWarp");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission)
					|| p.getName().equalsIgnoreCase("Punox") || p.getName().equalsIgnoreCase("OfficialPunox"))) {
				try {
					if (args.length == 1) {
						if (args[0].equalsIgnoreCase("1.6")) {
							p.sendMessage("§8§m----------------[§r §4§lUpdate Log §7| §e§l1.6 §8§m]----------------");
							p.sendMessage("");
							p.sendMessage("§8§l» §c§lBUGFIXES§7:");
							p.sendMessage("");
							p.sendMessage("  §8§l» §e§lLICENSE KEY §8(§7Basic license errors have been fixed§8)");
							p.sendMessage("  §8§l» §e§lVANISH §8(§7Visibility bugs have been fixed§8)");
							p.sendMessage("  §8§l» §e§lCoinsSystem §8(§7Money display errors have been fixed§8)");
							p.sendMessage("  §8§l» §e§lPaymentSystem §8(§7Incorrectly issued messages were corrected§8)");
							p.sendMessage("  §8§l» §e§lSpawnMob §8(§7Command could not be executed§8)");
							p.sendMessage("");
							p.sendMessage("§8§l» §a§lNEW FUNCTIONS§7:");
							p.sendMessage("");
							p.sendMessage("  §8§l» §e§lUPDATELOG §8(§7An update log has been added to track the functionality of the plugin.");
							p.sendMessage("  §8§l» §e§lCOINSAPI §8(§7A clear CoinsAPI has been added to work with other plugins.§8)");
							p.sendMessage("");
							p.sendMessage("§8§m------------------------------------------------");
							p.sendMessage("");

						}

						if (args[0].equalsIgnoreCase("1.7")) {
							p.sendMessage("§8§m----------------[§r §4§lUpdate Log §7| §e§l1.7 §8§m]----------------");
							p.sendMessage("");
							p.sendMessage("§8§l» §c§lBUGFIXES§7:");
							p.sendMessage("");
							p.sendMessage("  §8§l» §e§lSPEED §8(§7The speed can now be reset.§8)");
							p.sendMessage("");
							p.sendMessage("§8§l» §a§lNEW FUNCTIONS§7:");
							p.sendMessage("");
							p.sendMessage("  §8§l» §e§lINSPECTOR §8(§7You can now inspect players and have a better overview of your server. This feature will be extended in the future.§8)");
							p.sendMessage("  §8§l» §e§lGIVE §8(§7You can now dispense items with a simple command.§8)");
							p.sendMessage("  §8§l» §e§lITEM §8(§7You can now dispense items to yourself.§8)");
							p.sendMessage("");
							p.sendMessage("§8§m------------------------------------------------");
							p.sendMessage("");

						}

						if (args[0].equalsIgnoreCase("1.8")) {
							p.sendMessage("§8§m----------------[§r §4§lUpdate Log §7| §e§l1.8 §8§m]----------------");
							p.sendMessage("");
							p.sendMessage("§8§l» §c§lBUGFIXES§7:");
							p.sendMessage("");
							p.sendMessage("  §8§l» §e§lCOINS §8(§7Coins are now displayed and calculated correctly.§8)");
							p.sendMessage("  §8§l» §e§lMSG §8(§7Errors in higher server versions have been fixed.§8)");
							p.sendMessage("  §8§l» §e§lFLY §8(§7Fly permissions have been splitted into 2 areas so its more dynamic now.§8)");
							p.sendMessage("  §8§l» §e§lWARPS §8(§7Uppercase and lowercase letters are now ignored, as you have requested.§8)");
							p.sendMessage("");
							p.sendMessage("§8§l» §a§lNEW FUNCTIONS§7:");
							p.sendMessage("");
							p.sendMessage("  §8§l» §e§lWORLDSYSTEM §8(§7You can now create and modify worlds as you can teleport to them. This feature will be extended in the future.§8)");
							p.sendMessage("");
							p.sendMessage("§8§m------------------------------------------------");
							p.sendMessage("");

						}
						
						if (args[0].equalsIgnoreCase("1.8.3")) {
							p.sendMessage("§8§m----------------[§r §4§lUpdate Log §7| §e§l1.8 §8§m]----------------");
							p.sendMessage("");
							p.sendMessage("§8§l» §c§lBUGFIXES§7:");
							p.sendMessage("");
							p.sendMessage("  §8§l» §e§lMONEY §8(§7Many bugs in MoneySystem have been fixed§8.)");
							p.sendMessage("  §8§l» §e§lHOMES §8(§7Each player will now have its own home file generated to keep the system clear§8.)");
							p.sendMessage("  §8§l» §e§lSPEED §8(§7Error messages have been fixed§8.)");
							p.sendMessage("  §8§l» §e§lFLY §8(§7Permission errors have been fixed§8.)");
							p.sendMessage("  §8§l» §e§lWARPS §8(§7Error messages have been fixed§8.)");
							p.sendMessage("  §8§l» §e§lMESSAGING SYSTEM §8(§7Wrong messages were corrected and improved§8.)");
							p.sendMessage("  §8§l» §e§lLICENSE SYSTEM §8(§7The system has been improved§8.)");
							p.sendMessage("");
							p.sendMessage("§8§l» §a§lNEW FUNCTIONS§7:");
							p.sendMessage("");
							p.sendMessage("  §8§l» §e§lHOMES §8(§7You get an overview of your homes with the command /homes§8.)");
							p.sendMessage("  §8§l» §e§lHOMESAPI §8(§7A HomesAPI has now been released and is fully functional§8.)");
							p.sendMessage("");
							p.sendMessage("§8§m------------------------------------------------");
							p.sendMessage("");

						}
						
						if (args[0].equalsIgnoreCase("1.9")) {
							p.sendMessage("§8§m----------------[§r §4§lUpdate Log §7| §e§l1.9 §8§m]----------------");
							p.sendMessage("");
							p.sendMessage("§8§l» §c§lBUGFIXES§7:");
							p.sendMessage("");
							p.sendMessage("  §8§l» §e§lWARPS §8(§7Some bugs have been reported and fixed§8.)");
							p.sendMessage("  §8§l» §e§lMONEY §8(§7Some bugs have been reported and fixed§8.)");
							p.sendMessage("  §8§l» §e§lMESSAGING SYSTEM §8(§7Wrong messages were corrected and improved§8.)");
							p.sendMessage("  §8§l» §e§lLICENSE SYSTEM §8(§7The system has been improved§8.)");
							p.sendMessage("");
							p.sendMessage("§8§l» §a§lNEW FUNCTIONS§7:");
							p.sendMessage("");
							p.sendMessage("  §8§l» §e§lMYSQL §8(§7The CoinsAPI has been redesigned and a mysql.yml has been added§8.)");
							p.sendMessage("");
							p.sendMessage("§8§m------------------------------------------------");
							p.sendMessage("");

						}
						
						if (args[0].equalsIgnoreCase("2.0")) {
							p.sendMessage("§8§m----------------[§r §4§lUpdate Log §7| §e§l1.9 §8§m]----------------");
							p.sendMessage("");
							p.sendMessage("§8§l» §e§lThis log will be added after completing this release§8.");
							p.sendMessage("");
							p.sendMessage("§8§m------------------------------------------------");
							p.sendMessage("");

						}
					} else {
						throw new NullPointerException();
					}

				} catch (NullPointerException e) {
					p.sendMessage("§8§m------------------[§r §4§lUpdate Log §8§m]------------------");
					p.sendMessage("");
					p.sendMessage("§8§l» §c§lAVAILABLE LOGS§7:");
					p.sendMessage("");
					p.sendMessage("  §8§l» §e§l2.0 §8(§7§lCURRENT VERSION§8)");
					p.sendMessage("  §8§l» §e§l1.9");
					p.sendMessage("  §8§l» §e§l1.8.3");
					p.sendMessage("  §8§l» §e§l1.8");
					p.sendMessage("  §8§l» §e§l1.7");
					p.sendMessage("  §8§l» §e§l1.6");
					p.sendMessage("");
					p.sendMessage("§8§m------------------------------------------------");
					p.sendMessage("");
					p.sendMessage(this.prefix + usage + " §7§l/updatelog [Version]§7.");
				}

			} else {
				p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
			}

		}
		return false;
	}
}
