package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MessageCMD implements CommandExecutor {

    private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.MSG");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	private String msgtome = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.MSG.ToMe");
	private String msgtoother = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.MSG.ToOther");

	private String msg2 = "";

    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        Player p = (Player) cs;
        if ((cs instanceof Player)) {
        	if(p.hasPermission(permission) || (p.hasPermission(adminpermission))) {
	            if (!(args.length >= 1)) {
	                p.sendMessage(this.prefix + usage + " §7§l/MSG [Player] [Message]");
	            } else {
	                try {
	                    for (int msg = 1; msg < args.length; msg++) {
	                        this.msg2 = (this.msg2 + args[msg] + " ");
	                    }
	                    Player t = Bukkit.getPlayer(args[0]);
	                    if (p != t) {
	                        t.sendMessage(this.prefix + msgtome.replaceAll("%player%", p.getName())
	                                + this.msg2);
	                        p.sendMessage(this.prefix + msgtoother.replaceAll("%player%", t.getName())
	                                + this.msg2);
	                    } else {
	                        p.sendMessage(this.prefix + "§7§lYou can't write yourself...");
	                    }
	                    this.msg2 = " ";
	                } catch (Exception e) {
	                    Player t = Bukkit.getPlayer(args[0]);
	                    p.sendMessage(this.prefix + usage + " §7§l/MSG [Player] [Message]");
	                    if (!t.isOnline()) {
	                        p.sendMessage(this.prefix + "§7§lThat player isn't online...");
	                    } else {
	                        p.sendMessage(this.prefix
	                                + "§4§lDelete the §e§lsettings.yml §4§lof §e§lEssentialsReloaded §4§lonce to fix this§7!");
	                    }
	                }
	            }
        	} else {
                p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
        	}
        } else {
            cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
        }
        return false;
    }
}