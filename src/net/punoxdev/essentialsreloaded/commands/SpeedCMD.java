package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SpeedCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.Speed");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if (args[0] != null) {
				if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission))) {
					try {
						String level = args[0];
						int speed = Integer.parseInt(level);
						if (speed <= 1) {
							p.getActivePotionEffects().clear();
							p.removePotionEffect(PotionEffectType.SPEED);
							p.sendMessage(this.prefix + "§7§lYour speed has been reset...");
						} else {
							p.addPotionEffect(
									new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, speed, true, false));
							p.sendMessage(
									this.prefix + "§7§lYour speed is now on level §e§l" + speed);
						}
					} catch (NullPointerException e) {
						p.sendMessage(this.prefix + usage + " §7§l/speed [Integer]§7.");
					}
				} else {
					p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
				}
			} else {
				p.sendMessage(this.prefix + usage + " §7§l/speed [Integer]§7.");
			}
		}
		return false;
	}

}
