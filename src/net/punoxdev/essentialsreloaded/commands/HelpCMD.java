package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HelpCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");

	private String line1 = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Help.1");
	private String line2 = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Help.2");

	private String spacer = "";

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		p.sendMessage(prefix + line1);
		p.sendMessage(spacer);
		p.sendMessage(line2);
		p.sendMessage(spacer);
		p.sendMessage(prefix + line1);
		return false;
	}
}