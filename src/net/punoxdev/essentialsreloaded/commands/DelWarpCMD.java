package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DelWarpCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.WarpSystem.SetWarp");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission))) {
				if (args.length == 0) {
					p.sendMessage(this.prefix + this.usage + " §7§l/delwarp [Name]");
				} else if (args.length == 1) {
					try {
						main.getFileRepository().getWarpsConfiguration().load(main.getFileRepository().getWarpsFile());
					} catch (Exception localException) {}
					int id = main.getFileRepository().getWarpsConfiguration().getKeys(false).size();
					if (main.getFileRepository().getWarpsConfiguration().contains(args[0].toLowerCase() + ".world")) {


						main.getFileRepository().getWarpsConfiguration().set(id + ".name", null);

						main.getFileRepository().getWarpsConfiguration().set(args[0].toLowerCase() + ".name", null);
						main.getFileRepository().getWarpsConfiguration().set(args[0].toLowerCase() + ".world", null);
						main.getFileRepository().getWarpsConfiguration().set(args[0].toLowerCase() + ".x", null);
						main.getFileRepository().getWarpsConfiguration().set(args[0].toLowerCase() + ".y", null);
						main.getFileRepository().getWarpsConfiguration().set(args[0].toLowerCase() + ".z", null);
						main.getFileRepository().getWarpsConfiguration().set(args[0].toLowerCase() + ".pitch", null);
						main.getFileRepository().getWarpsConfiguration().set(args[0].toLowerCase() + ".yaw", null);

						try {
							main.getFileRepository().getWarpsConfiguration().save(main.getFileRepository().getWarpsFile());
							p.sendMessage(this.prefix + "§7§lYou §a§lsuccessfully §7§lremoved the warp §e§l" + args[0].toLowerCase());
						} catch (Exception localException1) {}
					} else {
						p.sendMessage(this.prefix + "§7§lThe warp §6§l" + args[0] + " §7§ldoesnt exist.");
					}
				} else {
					p.sendMessage(this.prefix + usage + " §7§l/setwarp [Name]");
				}
			} else {
				p.sendMessage(this.prefix + main.getFileRepository().getWarpsConfiguration().getString("messages.NoPermission"));
			}
		} else {
			cs.sendMessage(this.prefix + "§e§lYou have to be a player to perform this action.");
		}
		return false;
	}
}
