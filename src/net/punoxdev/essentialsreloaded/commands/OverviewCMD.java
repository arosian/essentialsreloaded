package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class OverviewCMD implements CommandExecutor{

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	String prefix = MessageAPI.getMessageFromConfig(fileRepository.getMessagesConfiguration(), "messages.Prefix");
	String spacer = "";
	String free = "§7§l■";
	String used = "§4§l■";

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		String temp = "";
		p.sendMessage(prefix.replaceAll("&", "§") + "§e§lOverview");
		p.sendMessage(spacer);
		for(int i = 0; i <= ((getUsedMemoryInPercent() / 10) * 2); i++) {
			temp += used;
		}
		for(int i = 0; i <= ((10 - (getUsedMemoryInPercent() / 10)) * 2); i++){
			temp += free;
		}
		p.sendMessage("§e§lMemory Usage§8§l: " + temp);
		//p.sendMessage("§r§8" + ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) + "§b§lMB §8/ " + Runtime.getRuntime().totalMemory() + "§b§lMB"));
		
		return false;
	}
	
	private static long getUsedMemoryInPercent() {
		return ((((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1000000) * 100) / (Runtime.getRuntime().totalMemory() / 1000000));
	}
	
	private static String getMemoryStatus() {
		if(getUsedMemoryInPercent() < 20) return "§6§lPERFECT";
		else if(getUsedMemoryInPercent() < 40) return "§a§lGOOD";
		else if(getUsedMemoryInPercent() < 80) return "§c§lWARNING";
		else if(getUsedMemoryInPercent() < 80) return "§4§lBAD";
		return "";
	}
}
