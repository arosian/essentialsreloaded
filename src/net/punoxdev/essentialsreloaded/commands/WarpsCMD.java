package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WarpsCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	String prefix = MessageAPI.getMessageFromConfig(fileRepository.getMessagesConfiguration(), "messages.Prefix");
	String usage = MessageAPI.getMessageFromConfig(fileRepository.getMessagesConfiguration(), "messages.Usage");

	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		try {
			try {
				main.getFileRepository().getWarpsConfiguration().load(main.getFileRepository().getWarpsFile());
			}catch(Exception e) {} 
			Player p = (Player) cs;
			if ((cs instanceof Player)) {
				if (args.length == 0) {
					if (main.getFileRepository().getWarpsConfiguration().getKeys(false).size() < 1) {
						p.sendMessage(this.prefix + "§6§lWarps & Locations");
						p.sendMessage("§8§m----------------------------");
						p.sendMessage("§4§lNo warps have been created");
						p.sendMessage("§8§m----------------------------");
						return true;
					}
					p.sendMessage(this.prefix + "§6§lWarps & Locations");
					p.sendMessage("§8§m--------------------------------------------");
						for(String s : main.getFileRepository().getWarpsConfiguration().getKeys(false)) {
							if(main.getFileRepository().getWarpsConfiguration().getString(s + ".world") != null) {
								p.sendMessage("§8§l» §e§l" + s + " §8(§c§oWorld§7: " + main.getFileRepository().getWarpsConfiguration().getString(s + ".world") + "§8)");
						}
					}
					p.sendMessage("§8§m--------------------------------------------");	
				} else {
					p.sendMessage(this.prefix + usage + " §7§l/warps");
				}
			}
		}catch(NullPointerException e) {}
		return false;
	}
}
