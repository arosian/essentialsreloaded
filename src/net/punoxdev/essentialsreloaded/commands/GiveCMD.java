package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class GiveCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.ItemSystem.give");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	public static HashMap<Player, Boolean> checkfly = new HashMap<>();

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission))) {
				try {
					Player t = Bukkit.getPlayer(args[0]);
					Material item = Material.matchMaterial(args[1]);
					int itemCount = Integer.valueOf(args[2]);
					if (t == null) {
						p.sendMessage(this.prefix + "§7§lThat player isn't online...");
						return true;
					}

					if (item == null) {
						p.sendMessage(
								this.prefix + "§7§lUnknown item §8[§c§o" + args[1] + "§r§8]");
					}

					ItemStack itemStack = new ItemStack(item, itemCount);
					t.getInventory().addItem(itemStack);
					p.sendMessage(this.prefix + "§7§lSuccesfully added §e§l" + itemCount + "§e§lx "
							+ args[1] + " §7§lto the inventory of §e§l" + t.getName() + "§7§l...");

				} catch (Exception e) {
					p.sendMessage(this.prefix + usage + " §7§l/give [Player] [Item] [Integer]");
				}
			} else {
				p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
			}
		}
		return false;

	}
}
