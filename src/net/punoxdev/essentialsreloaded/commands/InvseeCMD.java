package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class InvseeCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.Invsee");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if (p.hasPermission("permission")) {
				if (args.length == 0) {
					Inventory tinv = p.getInventory();
					p.openInventory(tinv);
					p.sendMessage(this.prefix + "§7§lYou're now watching your own §e§linventory");
				}

				else if (args.length == 1) {
					Player t = Bukkit.getPlayer(args[0]);
					try {
						p.openInventory(t.getInventory());
						p.sendMessage(this.prefix + "§7§lYou're now watching the inventory of §e§l" + t.getName() + "");
					} catch (Exception e) {
						p.sendMessage(this.prefix + "§7§lThat player isn't online...");
					}
				} else {
					p.sendMessage(this.prefix + usage + " §7§l/invsee <Player>");
				}
			}
		} else {
			cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
		}
		return false;
	}
}
