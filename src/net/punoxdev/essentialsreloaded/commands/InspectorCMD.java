package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.ArrayList;

public class InspectorCMD implements CommandExecutor, Listener {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.Inspector.use");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	private static ArrayList<Player> inspectorPlayers = new ArrayList<>();

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission))) {
				try {
					if (!inspectorPlayers.contains(p)) {
						inspectorPlayers.add(p);
						p.sendMessage(
								this.prefix + "§7§lYou are now inspecting §e§lall §7§lplayers...");
					} else {
						inspectorPlayers.remove(p);
						p.sendMessage(this.prefix
								+ "§7§lYou are now stopping inspecting §e§lall §7§lplayers...");
					}
				} catch (Exception e) {
					p.sendMessage(this.prefix + usage + " §7§l/inspector");
				}
			} else {
				p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
			}
		}
		return false;

	}

	// INSPECTOR EVENTS
	@EventHandler
	public void onBlockBreak(final BlockBreakEvent event) {
		inspectorPlayers.forEach(isp -> isp
				.sendMessage(this.prefix + "§7§lThe player §e§l" + event.getPlayer().getName()
						+ " §7§lhas §4§ldestroyed §7§la block §8(§c§o" + event.getBlock().getType() + "§8)"));
	}

	@EventHandler
	public void onBlockPlace(final BlockPlaceEvent event) {
		inspectorPlayers.forEach(isp -> isp
				.sendMessage(this.prefix + "§7§lThe player §e§l" + event.getPlayer().getName()
						+ " §7§lhas §a§lplaced §7§la block  §8(§c§o" + event.getBlock().getType() + "§8)"));
	}

	@EventHandler
	public void onCommandExcute(final AsyncPlayerChatEvent event) {
		inspectorPlayers.forEach(isp -> isp.sendMessage(this.prefix + "§7§lThe player §e§l"
				+ event.getPlayer().getName() + " §7§lexecuted a command §8(§c§o" + event.getMessage() + "§8)"));
	}
}
