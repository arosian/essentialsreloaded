package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GameModeCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.GameMode.*");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission))) {
				if (args.length == 0) {
					p.sendMessage(this.prefix + usage + " §7§l/gm [0|1|2|3] <Player>");
				} else if (args.length == 1) {
					if (args[0].equalsIgnoreCase("0")) {
						p.setGameMode(GameMode.SURVIVAL);
						p.sendMessage(this.prefix + "§7§lYou are now in GameMode §e§l"
								+ p.getGameMode() + "");
					} else if (args[0].equalsIgnoreCase("1")) {
						p.setGameMode(GameMode.CREATIVE);
						p.sendMessage(this.prefix + "§7§lYou are now in GameMode §e§l"
								+ p.getGameMode() + "");
					} else if (args[0].equalsIgnoreCase("2")) {
						p.setGameMode(GameMode.ADVENTURE);
						p.sendMessage(this.prefix + "§7§lYou are now in GameMode §e§l"
								+ p.getGameMode() + "");
					} else if (args[0].equalsIgnoreCase("3")) {
						p.setGameMode(GameMode.SPECTATOR);
						p.sendMessage(this.prefix + "§7§lYou are now in GameMode §e§l"
								+ p.getGameMode() + "");
					} else {
						p.sendMessage(this.prefix + usage + " §7§l/gm [0|1|2|3] <Player>");
					}
				} else if (args.length == 2) {
					Player t = Bukkit.getPlayer(args[1]);

					try {
						if (args[0].equalsIgnoreCase("0")) {
							t.setGameMode(GameMode.SURVIVAL);
							p.sendMessage(this.prefix + "§7§lYou are now in GameMode §e§l"
									+ p.getGameMode() + "");
							p.sendMessage(this.prefix + "§7§lThe player named §e§l" + t.getName()
									+ "§7§lis now in GameMode §6§l" + t.getGameMode() + "");
						} else if (args[0].equalsIgnoreCase("1")) {
							t.setGameMode(GameMode.CREATIVE);
							t.sendMessage(this.prefix + "§7§lYou are now in GameMode §e§l"
									+ p.getGameMode() + "");
							p.sendMessage(this.prefix + "§7§lThe player named §e§l" + t.getName()
									+ " §7§lis now in GameMode §6§l" + t.getGameMode() + "");
						} else if (args[0].equalsIgnoreCase("2")) {
							t.setGameMode(GameMode.ADVENTURE);
							t.sendMessage(this.prefix + "§7§lYou are now in GameMode §e§l"
									+ p.getGameMode() + "");
							p.sendMessage(this.prefix + "§7§lThe player named §e§l" + t.getName()
									+ "§7§lis now in GameMode §6§l" + t.getGameMode() + "");
						} else if (args[0].equalsIgnoreCase("3")) {
							t.setGameMode(GameMode.SPECTATOR);
							t.sendMessage(this.prefix + "§7§lYou are now in GameMode §e§l"
									+ p.getGameMode() + "");
							p.sendMessage(this.prefix + "§7§lThe player named §e§l" + t.getName()
									+ "§7§lis now in GameMode §6§l" + t.getGameMode() + "");
						} else {
							t.sendMessage(this.prefix + usage + " §7§l/gm [0|1|2|3] <Player>");
						}
					} catch (Exception e) {
						p.sendMessage(this.prefix + "§7§lThat player isn't online...");
					}
				} else {
					p.sendMessage(this.prefix + usage + " §7§l/gm [0|1|2|3] <Player>");
				}
			} else {
				p.sendMessage(this.prefix + main.getFileRepository().getSettingsConfiguration().getString("messages.NoPermission"));
			}
		} else {
			cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
		}

		return false;
	}
}
