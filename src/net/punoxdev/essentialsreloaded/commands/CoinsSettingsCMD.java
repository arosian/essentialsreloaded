
package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.CoinsAPI;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CoinsSettingsCMD implements CommandExecutor {

    private Main main = Main.getInstance();

    private FileRepository fileRepository = main.getFileRepository();

    private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
    private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

    private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.PaymentSystem.CoinSettings");
    private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        Player p = (Player) cs;

        if ((cs instanceof Player)) {
            if (p.hasPermission(permission) || p.hasPermission(adminpermission)) {
                try {
                    main.getFileRepository().getCoinsConfiguration().load(main.getFileRepository().getCoinsFile());
                } catch (Exception localException1) {
                }

                if (args.length == 0) {
                    p.sendMessage(this.prefix + usage + " §7§l/coinssettings [set|add|del|get] [Player] [Integer]§7.");
                } else if (args.length == 1) {
                    p.sendMessage(this.prefix + usage + " §7§l/coinssettings [set|add|del|get] [Player] [Integer]§7.");
                } else if (args.length == 3) {
                    Player t = Bukkit.getPlayer(args[1]);

                    String anzahl = args[2];
                    Player target = Bukkit.getPlayer(args[1]);
                    int betrag = Integer.parseInt(anzahl);
                        try {
                            if (args[0].equalsIgnoreCase("set")) {
                                p.sendMessage(this.prefix + "§7You successfully set the coins from §e§l" + t.getName() + " §7to §a " + args[2] + "§7...");
                                CoinsAPI.setCoins(target.getName(), betrag);
                            } else if (args[0].equalsIgnoreCase("add")) {
                                p.sendMessage(this.prefix + "§7You successfully expanded the coins from §e§l" + t.getName() + " §7with §e§l" + args[2] + "§7...");
                                CoinsAPI.addCoins(p.getName(), betrag);
                            } else if (args[0].equalsIgnoreCase("del")) {
                                p.sendMessage(this.prefix + "§7You successfully removed §e§l" + args[2] + " §7coins from §e§l" + t.getName() + "...");
                                CoinsAPI.removeCoins(p.getName(), betrag);
                            } else {
                                p.sendMessage(this.prefix + usage + " §7§l/coinssettings [set|add|del|get] [Player] [Integer]§7.");
                            }
                    } catch (Exception e) {
                        p.sendMessage(this.prefix + "§7§lThat player isn't online...");
                    }
                } else if (args.length == 2) {
                    Player t = Bukkit.getPlayer(args[1]);
                    if (args[0].equalsIgnoreCase("get")) {
                        p.sendMessage(this.prefix + "§7The player §e§l" + t.getName() + " §r§7currently has §e§l" + CoinsAPI.getCoins(p.getName()) + " Coins§7§l...");
                    } else {
                        p.sendMessage(this.prefix + usage + " §7§l/coinssettings [set|add|del|get] [Player] [Integer]§7.");
                    }
                }
            } else {
                p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
            }

        } else {
            cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
        }

        return false;
    }
}
