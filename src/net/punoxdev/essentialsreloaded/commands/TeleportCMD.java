package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportCMD implements CommandExecutor {

    private Main main = Main.getInstance();

    private FileRepository fileRepository = main.getFileRepository();

    String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
    String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

    private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.Teleport");
    private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        Player p = (Player) cs;
        if ((cs instanceof Player)) {
            if (p.hasPermission(permission) || p.hasPermission(adminpermission)) {
                if (cmd.getName().equalsIgnoreCase("tp") || cmd.getName().equalsIgnoreCase("tpo")) {
                    if (args.length == 0) {
                        p.sendMessage(this.prefix + usage + " §7§l/tp [Player]");
                    } else if (args.length == 1) {
                        Player t = Bukkit.getPlayer(args[0]);

                        try {
                            p.teleport(t);
                            p.sendMessage(this.prefix + "§7§lYou have been teleported to §e§l" + t.getName() + "");
                        } catch (Exception e) {
                            p.sendMessage(this.prefix + "§7§lThat player isn't online...");
                        }
                    } else if (args.length == 3) {
                        p.sendMessage(this.prefix + usage + " §7§l/tp [Player]");
                    } else {

                    }
                }
            } else {
                p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
            }

            if ((p.hasPermission(permission)) && (cmd.getName().equalsIgnoreCase("tphere"))) {
                if (args.length == 0) {
                    p.sendMessage(this.prefix + usage + " §7§l/tphere [Player]");
                } else if (args.length == 1) {
                    Player t = Bukkit.getPlayer(args[0]);

                    try {
                        t.teleport(p);
                        p.sendMessage(this.prefix + "§7§lYou teleported §e§l" + t.getName() + " §7§lto your location");
                    } catch (Exception e) {
                        p.sendMessage(this.prefix + "§7§lThat player isn't online...");
                    }
                } else {
                    p.sendMessage(this.prefix + usage + " §7§l/tphere [Player]");
                }
            }
        } else {
            cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
        }

        return false;
    }
}
