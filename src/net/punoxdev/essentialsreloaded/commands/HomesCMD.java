package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.HomesAPI;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;


public class HomesCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if (args.length == 0) {
				List<String> home = HomesAPI.getListOfHomes(p);
				if (home.isEmpty()) {
					p.sendMessage(this.prefix + "§6§lHomes");
					p.sendMessage("§8§m----------------------------");
					p.sendMessage("§4§lNo homes have been created");
					p.sendMessage("§8§m----------------------------");
					return true;
				}
				p.sendMessage(this.prefix + "§6§lHomes");
				p.sendMessage("§8§m--------------------------------------------");
				
				List<String> str = new ArrayList<>();
				List<String> str2 = new ArrayList<>();
				for (int i = 0; i < home.size(); i++) {
					Location loc = HomesAPI.getHome(p, home.get(i));
					str.add(home.get(i));
					str2.add(loc.getWorld().getName());
				}
				for(int i = 0; i < str.size(); i++) {
					p.sendMessage("§8§l» §e§l" + str.get(i) + " §8(§c§oWorld§7: " + str2.get(i) + "§8)");
				}
				p.sendMessage("§8§m--------------------------------------------");
			} else {
				p.sendMessage(this.prefix + usage + " §7§l/warps");
			}

		} else {
			cs.sendMessage("//");
		}
		return false;
	}
}
