package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WarpCMD implements CommandExecutor {

    private Main main = Main.getInstance();

    private FileRepository fileRepository = this.main.getFileRepository();

    private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
    private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

    private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.WarpSystem.Warp");
    private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

    private int id = this.main.getFileRepository().getWarpsConfiguration().getKeys(false).size();

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        Player p = (Player) cs;
        if ((cs instanceof Player)) {
            if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission))) {
                if (args.length == 0) {
                    p.sendMessage(this.prefix + usage + " §7§l/warp [Name]§7.");
                } else if (args.length == 1) {
                    try {
                        main.getFileRepository().getWarpsConfiguration().load(main.getFileRepository().getWarpsFile());
                    } catch (Exception localException) {
                    }

                    if (main.getFileRepository().getWarpsConfiguration().contains(args[0].toLowerCase() + ".world")) {
                        try {
                            String world = main.getFileRepository().getWarpsConfiguration().getString(args[0].toLowerCase() + ".world");
                            double x = main.getFileRepository().getWarpsConfiguration().getDouble(args[0].toLowerCase() + ".x");
                            double y = main.getFileRepository().getWarpsConfiguration().getDouble(args[0].toLowerCase() + ".y");
                            double z = main.getFileRepository().getWarpsConfiguration().getDouble(args[0].toLowerCase() + ".z");
                            double pitch = main.getFileRepository().getWarpsConfiguration().getDouble(args[0].toLowerCase() + ".pitch");
                            double yaw = main.getFileRepository().getWarpsConfiguration().getDouble(args[0].toLowerCase() + ".yaw");

                            Location loc = new Location(Bukkit.getWorld(world), x, y, z);
                            loc.setPitch((float) pitch);
                            loc.setYaw((float) yaw);

                            p.teleport(loc);
                            p.sendMessage(this.prefix + "§7§lYou have been teleported to §e§l" + args[0] + "");

                        } catch (NullPointerException e) {
                            p.sendMessage(this.prefix + "§7§lThe warp §e§l" + args[0] + " §7§ldoesn't exist");
                        }
                    } else {
                        p.sendMessage(this.prefix + "§7§lThe warp §e§l" + args[0] + " §7§ldoesn't exist");
                    }
                } else {
                    p.sendMessage(this.prefix + usage + " §7§l/warp [Name]§7.");
                }
            } else {
                p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
            }
        } else {
            cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
        }
        return false;
    }
}
