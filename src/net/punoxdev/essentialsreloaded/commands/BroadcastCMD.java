package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BroadcastCMD implements CommandExecutor {

	private Main main = Main.getInstance();
	
	private FileRepository fileRepository = main.getFileRepository();
	
	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.Broadcast");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	String msg2 = " ";

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if ((p.hasPermission(permission)) || (p.hasPermission(adminpermission))) {
				if (args.length >= 1) {
					try {

						for (int msg = 0; msg < args.length; msg++) {
							this.msg2 = (this.msg2 + args[msg] + " ");
						}

						for (Player all : Bukkit.getOnlinePlayers()) {
							all.sendMessage(this.prefix + this.msg2.replaceAll("&", "§"));
						}

						this.msg2 = " ";

					} catch (Exception localException) {
					}
				} else {
					p.sendMessage(this.prefix + usage + " §7§l/broadcast [Message]");
				}
			} else {
				p.sendMessage(this.prefix + MessageAPI.getMessageFromConfig(fileRepository.getMessagesConfiguration(), "messages.NoPermission"));
			}
		} else {
			cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
		}

		return false;
	}
}