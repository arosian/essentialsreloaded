package net.punoxdev.essentialsreloaded.commands;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class TeleportOtherCMD implements CommandExecutor {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");
	private String usage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Usage");

	private String permission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.TPA");
	private String adminpermission = MessageAPI.getMessageFromConfig(this.fileRepository.getSettingsConfiguration(), "settings.permissions.*");

	public static ArrayList<Player> tpa = new ArrayList();

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		final Player p = (Player) cs;
		if ((cs instanceof Player)) {
			if (p.hasPermission(permission) || p.hasPermission(adminpermission)) {
				if (cmd.getName().equalsIgnoreCase("tpa")) {
					if (args.length == 0) {
						p.sendMessage(this.prefix + usage + " §7§l/tpa [Player]");
					} else if (args.length == 1) {
						final Player t = Bukkit.getPlayer(args[0]);
						try {
							if (p != t) {
								p.sendMessage(this.prefix + "§7§lTeleport-Request was sent to §e§l"
										+ t.getName() + "");
								t.sendMessage(this.prefix + "§6§lTeleport-Request §8| §e§l"
										+ p.getName() + "");
								t.sendMessage("§8§m-----------------------------------------------");
								t.sendMessage(this.prefix + "§a§lAccept it with §e§l/tpaccept "
										+ p.getName());
								t.sendMessage(this.prefix + "§4§lDeny it with §e§l/tpdeny "
										+ p.getName());
								t.sendMessage("§8§m-----------------------------------------------");
								tpa.add(t);
								tpa.add(p);

								Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
									@Override
									public void run() {
										if ((TeleportOtherCMD.tpa.contains(t)) && (!TeleportOtherCMD.tpa.contains(p))) {
											TeleportOtherCMD.tpa.remove(t);
											TeleportOtherCMD.tpa.remove(p);
											t.sendMessage(TeleportOtherCMD.this.prefix
													+ "§7§lTeleport - Request from §e§l" + t.getName()
													+ " §7§lhas expired...");
											t.sendMessage(TeleportOtherCMD.this.prefix
													+ "§7§lTeleport - Request from §e§l" + t.getName()
													+ " §7§lhas expired...");
										}

									}
								}, 600L);

							} else {
								p.sendMessage(
										this.prefix + "§7§lYou cannot do this with yourself");
							}
						} catch (Exception e) {
							p.sendMessage(this.prefix + "§7§lThat player isn't online...");
						}
					} else {
						p.sendMessage(this.prefix + usage + " §7§l/tpa [Player]");
					}
				} else if (cmd.getName().equalsIgnoreCase("tpaccept")) {
					if (args.length == 0) {
						p.sendMessage(this.prefix + usage + " §7§l/tpaccept [Player]");
					} else if (args.length == 1) {
						Player t = Bukkit.getPlayer(args[0]);
						try {
							if (p != t) {
								if ((tpa.contains(p)) && (tpa.contains(t))) {
									t.teleport(p);
									p.sendMessage(this.prefix
											+ "§7You accepted the §e§lTeleport - Request §7§lfrom §e§l" + t.getName());
									t.sendMessage(this.prefix + "§7The player §6§l" + p.getName()
											+ " §7§laccepted your §e§lTeleport - Request");

									tpa.remove(t);
									tpa.remove(p);
								} else {
									p.sendMessage(this.prefix + "§7Der Spieler §a" + t.getName()
											+ " §7hat dir keine TPA-Anfrage geschickt.");
								}
							} else {
								p.sendMessage(
										this.prefix + "§7§lYou cannot do this with yourself");
							}
						} catch (Exception e) {
							p.sendMessage(this.prefix + "§7§lThat player isn't online...");
						}
					} else {
						p.sendMessage(this.prefix + usage + " §7§l/tpaccept [Player]");
					}
				} else if (cmd.getName().equalsIgnoreCase("tpadeny")) {
					if (args.length == 0) {
						p.sendMessage(this.prefix + usage + " §7§l/tpadeny [Player]");
					} else if (args.length == 1) {
						Player t = Bukkit.getPlayer(args[0]);
						try {
							if (p != t) {
								if ((tpa.contains(p)) && (tpa.contains(t))) {

									p.sendMessage(this.prefix
											+ "§7You denied the §e§lTeleport - Request §7§lfrom §e§l" + t.getName());
									t.sendMessage(this.prefix + "§7The player §6§l" + p.getName()
											+ " §7§ldenied your §e§lTeleport - Request");

									tpa.remove(t);
									tpa.remove(p);
								} else {
									p.sendMessage(this.prefix + "§7Der Spieler §a" + t.getName()
											+ " §7hat dir keine TPA-Anfrage geschickt.");
								}
							} else {
								p.sendMessage(
										this.prefix + "§7§lYou cannot do this with yourself");
							}
						} catch (Exception e) {
							p.sendMessage(this.prefix + "§7§lThat player isn't online...");
						}
					} else {
						p.sendMessage(this.prefix + usage + " §7§l/tpadeny [Player]");
					}
				}
			} else {
				p.sendMessage(this.prefix + main.getFileRepository().getMessagesConfiguration().getString("messages.NoPermission"));
			}
		} else {
			cs.sendMessage(prefix + "§e§lYou have to be a player to perform this action.");
		}

		return false;
	}
}
