package net.punoxdev.essentialsreloaded.entities;

import javax.persistence.Entity;

import org.bukkit.command.CommandExecutor;

@Entity
public class Command {

    private String name;
    private String group;
    private Boolean enabled;
    private CommandExecutor commandExecutor;

    public Command(String name, String group, Boolean enabled, CommandExecutor commandExecutor) {
        this.name = name;
        this.group = group;
        this.enabled = enabled;
        this.commandExecutor = commandExecutor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    
    public CommandExecutor getCommandExecutor() {
    	return commandExecutor;
    }
    
    public void setCommandExecutor(CommandExecutor commandExecutor) {
    	this.commandExecutor = commandExecutor;
    }
    
    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
