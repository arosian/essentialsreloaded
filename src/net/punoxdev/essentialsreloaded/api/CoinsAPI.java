package net.punoxdev.essentialsreloaded.api;

import net.punoxdev.essentialsreloaded.Main;

public class CoinsAPI {
	
	public static void createUser(String name, Integer coins) {
		if(Main.getInstance().isMySQLEnabled()) {
			new CoinsAPIMySQL().createPlayer(name, coins);
		} else {
			new CoinsAPIFile().createPlayer(name, coins);
		}
	}
	
	public static void setCoins(String name, Integer coins) {
		if(Main.getInstance().isMySQLEnabled()) {
			new CoinsAPIMySQL().setCoins(name, coins);
		} else {
			new CoinsAPIFile().setCoins(name, coins);
		}
	}
	
	public static int getCoins(String name) {
		if(Main.getInstance().isMySQLEnabled()) {
			return new CoinsAPIMySQL().getCoins(name);
		} else {
			return new CoinsAPIFile().getCoins(name); 
		}
	}
	
	public static String getCoinsString(String name) {
		if(Main.getInstance().isMySQLEnabled()) {
			return new CoinsAPIMySQL().getCoins(name) + "";
		} else {
			return new CoinsAPIFile().getCoins(name) + ""; 
		}
	}
	
	public static void addCoins(String name, Integer coins) {
		if(Main.getInstance().isMySQLEnabled()) {
		 new CoinsAPIMySQL().setCoins(name, new CoinsAPI().getCoins(name) + coins);
		} else {
			new CoinsAPIFile().addCoins(name, coins);
		}
	}
	
	public static void removeCoins(String name, Integer coins) {
		if(Main.getInstance().isMySQLEnabled()) {
			new CoinsAPIMySQL().setCoins(name, new CoinsAPI().getCoins(name) - coins);
		}else {
			new CoinsAPIFile().removeCoins(name, coins);
		}
	}
}
