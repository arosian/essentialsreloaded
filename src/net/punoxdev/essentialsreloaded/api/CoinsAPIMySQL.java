package net.punoxdev.essentialsreloaded.api;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.punoxdev.essentialsreloaded.Main;

public class CoinsAPIMySQL {
	
	public int getCoins(String name) {
		int coins = 0;
		try {
			PreparedStatement ps = Main.getInstance().getMySQL().getConnection().prepareStatement("SELECT COINS FROM coins WHERE PLAYER = ?");
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
		    if (rs.next()) {
		    	return Integer.valueOf(rs.getInt("COINS"));
		    }
			rs.close();
			ps.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public void createPlayer(String name, Integer coins) {
			if(!playerExists(name)) {
				try {
					PreparedStatement ps = Main.getInstance().getMySQL().getConnection().prepareStatement("INSERT INTO coins (PLAYER, COINS) VALUES (?, ?) ON DUPLICATE KEY UPDATE PLAYER = ?");
					ps.setString(1, name);
					ps.setInt(2, coins);
					ps.setString(3, name);
					ps.executeUpdate();
					ps.close();
					Bukkit.getConsoleSender().sendMessage("User successfully created");
				} catch (SQLException e) {
					e.printStackTrace();
					Bukkit.getConsoleSender().sendMessage("Error whilst creating user...");
			}
		}
	}
	
	public void setCoins(String name, Integer coins) {
		try {
			PreparedStatement ps = Main.getInstance().getMySQL().getConnection().prepareStatement("UPDATE coins SET COINS = ? WHERE PLAYER = ?");
			ps.setInt(1, coins);
			ps.setString(2, name);
			ps.executeUpdate();
			ps.close();
			Bukkit.getConsoleSender().sendMessage("Successfully set coins to database...");
		}catch(SQLException e) {
			e.printStackTrace();
			Bukkit.getConsoleSender().sendMessage("Error whilst setting coins to database...");
		}
		
	}
	
	  public boolean playerExists(String name) {
	    try {
	      PreparedStatement ps = Main.getInstance().getMySQL().getConnection().prepareStatement("SELECT * FROM coins WHERE PLAYER = ?");
	      ps.setString(1, name);
	      ResultSet rs = ps.executeQuery();
	      if (rs.next()) {
	        if (rs.getString("PLAYER") != null) {
	          return true;
	        }
	        return false;
	      }
	      rs.close();
	      ps.close();
	    }
	    catch (SQLException var61)
	    {
	      var61.printStackTrace();
	    }
	    return false;
	  }
}
