package net.punoxdev.essentialsreloaded.api;

import net.punoxdev.essentialsreloaded.Main;
import net.punoxdev.essentialsreloaded.commands.WarpCMD;
import net.punoxdev.essentialsreloaded.entities.Command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;

public class CommandAPI {
	
	public static void sendCommandStatusToConsole(List<List<Command>> commandRepository) {
		List<String> checkedGroups = new ArrayList<>();
		for(Command command : commandRepository.get(0)) {
			if(!checkedGroups.contains(command.getGroup())) {
				if(command.getEnabled()) {
				  //Bukkit.getConsoleSender().sendMessage("§6§l" + command.getGroup() + " §7§l» §a§l✔");
					Bukkit.getConsoleSender().sendMessage("§6§l" + command.getGroup() + " §7§l* §a§lENABLED");
				  checkedGroups.add(command.getGroup());
			} else {
				//Bukkit.getConsoleSender().sendMessage("§6§l" + command.getGroup() + " §7§l» §4§l✘");
					Bukkit.getConsoleSender().sendMessage("§6§l" + command.getGroup() + " §7§l* §4§lDISABLED");
				checkedGroups.add(command.getGroup());
			} 
			}
		}
	}
	
    // Added soon. Created after 2.0 SECURITY RELEASE on Master Branch;
	// If someone sees this... I dont know why im making this plugin for free. 
	// Its just i have been creating Minecraft Networks for so many years and i gained so much experience doing that.
	// And i want to make the live easier for you ;)
	
    public static Boolean isCommandGroupActivated(String commandGroupName) {
        return Main.getInstance().getFileRepository().getSettingsConfiguration().getBoolean("settings.toggle." + commandGroupName);
    }

    public static void registerCommand(Command command) {
        if(!command.getEnabled()) {
            if(isCommandGroupActivated(command.getGroup())) {
                command.setEnabled(true);
                Main.getInstance().getCommand(command.getName()).setExecutor((CommandExecutor)command.getCommandExecutor());
            }else {
            	command.setEnabled(false);
            }
        } else {
        	 Main.getInstance().getCommand(command.getName()).setExecutor((CommandExecutor)command.getCommandExecutor());
        }
    }
    
    public static List<Command> getActivatedCommands(List<List<Command>> commandRepository) {
    	List<Command> activatedCommands = new ArrayList<>();
    	for(Command command : commandRepository.get(0)) {
    		if(command.getEnabled()) {
    			activatedCommands.add(command);
    		}
    	}
		return activatedCommands;
    }
    
    public static List<Command> getDisabledCommands(List<List<Command>> commandRepository) {
    	List<Command> disabledCommands = new ArrayList<>();
    	for(Command command : commandRepository.get(0)) {
    		if(!command.getEnabled()) {
    			disabledCommands.add(command);
    		}
    	}
    	return disabledCommands;
    }
    
    public static List<String> getNamesOfActivatedCommands(List<List<Command>> commandRepository) {
    	List<String> namesOfActivatedCommands = new ArrayList<>();
    	for(Command command : commandRepository.get(0)) {
    		if(command.getEnabled()) {
    			namesOfActivatedCommands.add(command.getName());
    		}
    	}
		return namesOfActivatedCommands;
    }
    
    public static List<String> getNamesOfDisabledCommands(List<List<Command>> commandRepository) {
    	List<String> namesOfDisabledCommands = new ArrayList<>();
    	for(Command command : commandRepository.get(0)) {
    		if(!command.getEnabled()) {
    			namesOfDisabledCommands.add(command.getName());
    		}
    	}
    	return namesOfDisabledCommands;
    }
    
}
