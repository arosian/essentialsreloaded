package net.punoxdev.essentialsreloaded.api;

import java.io.File;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class MessageAPI {
	
	File file = new File("plugins/EssentialsReloaded", "messages.yml");
	YamlConfiguration cfg = YamlConfiguration.loadConfiguration(this.file);
	
	public static String getMessageFromConfig(YamlConfiguration configuration, String key) {
		return configuration.getString(key).replaceAll("&", "§");
	}
	
	public static Boolean getBooleanFromConfig(YamlConfiguration configuration, String key) {
		return configuration.getBoolean(key);
	}
	
	public static Integer getIntegerFromConfig(YamlConfiguration configuration, String key) {
		return configuration.getInt(key);
	}
	
	public static void sendMessageToPlayer(Player p, String msg) {
		p.sendMessage(msg);
	}
	
	public static void sendMessageToSpecificPlayers(List<Player> pl, String msg) {
		for(Player p : pl) {
			p.sendMessage(msg);
		}
	}
	
	public static void broadcastMessage(String msg) {
		Bukkit.broadcastMessage(msg);
	}
}
