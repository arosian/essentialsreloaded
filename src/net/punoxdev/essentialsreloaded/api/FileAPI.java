package net.punoxdev.essentialsreloaded.api;

import java.io.File;
import java.io.IOException;

import net.punoxdev.essentialsreloaded.Main;
import org.bukkit.configuration.file.YamlConfiguration;

public class FileAPI {

    public static void createYamlConfiguration(String fileName) throws IOException {
        YamlConfiguration yamlConfiguration = getYamlConfiguration(fileName);
        yamlConfiguration.save(getFile(fileName));
    }

    public static YamlConfiguration getYamlConfiguration(String fileName) {
        return YamlConfiguration.loadConfiguration(getFile(fileName));
    }

    public static File getFile(String fileName) {
        return new File("plugins/EssentialsReloaded", fileName);
    }
}
