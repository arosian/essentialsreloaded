package net.punoxdev.essentialsreloaded.api;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryAPI {
	
	// This InventoryAPI has not been released yet so its not working...
	// Will be released in 2.0.
	
	List<String> loadedPlugins = new ArrayList<>();
	
	public static Inventory createChestInventory(String name, Integer size) {
		Inventory inv = Bukkit.createInventory(null, size, name);
		return inv;
	}
	
	public static Inventory setItemToInventory(Inventory inv, Material material, String name, Integer count, Integer index) {
		ItemStack x = new ItemStack(material, count);
		x.getItemMeta().setDisplayName(name);
		inv.setItem(4, x);
		return inv;
	}
	
	public static void openInventoryToPlayer(Inventory inv, Player p) {
		p.openInventory(inv);
	}
	
	public static void closeInventoryOfPlayer(Player p) {
		p.closeInventory();
	}
	
	public static void test() {
		Inventory inv = createChestInventory("testInventory", 9);
		inv = setItemToInventory(inv, Material.ENDER_CHEST, "testItem", 1, 4);
	}
	
	public static String s(String s) {
		return s;
	}
	
	public static void test2() {
		
}
	
}
