package net.punoxdev.essentialsreloaded.api;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import net.punoxdev.essentialsreloaded.Main;

public class CoinsAPIFile {

	private Main main = Main.getInstance();

	public static boolean fileExist() {
		if (Main.getInstance().getFileRepository().getCoinsFile().exists()) {
			return true;
		}
		return false;
	}
	
	public void createPlayer(String name, Integer coins) {
		if (!Main.getInstance().getFileRepository().getCoinsConfiguration().contains("coins." + name)) {
			Main.getInstance().getFileRepository().getCoinsConfiguration().set("coins." + name, coins);
			try {
				Main.getInstance().getFileRepository().getCoinsConfiguration().save(Main.getInstance().getFileRepository().getCoinsFile());
			} catch (Exception localException) {
			}
		}
	}

	public static int getCoins(String name) {
		try {
			Main.getInstance().getFileRepository().getCoinsConfiguration().load(Main.getInstance().getFileRepository().getCoinsFile());
		} catch (Exception localException) {
		}

		int coins = Main.getInstance().getFileRepository().getCoinsConfiguration().getInt("coins." + name);

		return coins;
	}

	public static String getCoinsString(Player p) {
		try {
			Main.getInstance().getFileRepository().getCoinsConfiguration().load(Main.getInstance().getFileRepository().getCoinsFile());
		} catch (Exception localException) {
		}

		return Main.getInstance().getFileRepository().getCoinsConfiguration().getString("coins." + p.getName());
	}

	public static void setCoins(String name, Integer coins) {
		try {
			Main.getInstance().getFileRepository().getCoinsConfiguration().load(Main.getInstance().getFileRepository().getCoinsFile());
		} catch (Exception localException) {
		}

		if ((fileExist()) && (Main.getInstance().getFileRepository().getCoinsConfiguration().contains("coins." + name))) {
			Main.getInstance().getFileRepository().getCoinsConfiguration().set("coins." + name, coins);
			try {
				Main.getInstance().getFileRepository().getCoinsConfiguration().save(Main.getInstance().getFileRepository().getCoinsFile());
			} catch (Exception localException1) {
			}
		}
	}

	public static void addCoins(String name, Integer coins) {
		try {
			Main.getInstance().getFileRepository().getCoinsConfiguration().load(Main.getInstance().getFileRepository().getCoinsFile());
		} catch (Exception localException) {
		}

		if ((fileExist()) && (Main.getInstance().getFileRepository().getCoinsConfiguration().contains("coins." + name))) {
			Main.getInstance().getFileRepository().getCoinsConfiguration().set("coins." + name, Main.getInstance().getFileRepository().getCoinsConfiguration().getInt("coins." + name) + coins);
			try {
				Main.getInstance().getFileRepository().getCoinsConfiguration().save(Main.getInstance().getFileRepository().getCoinsFile());
			} catch (Exception localException1) {
			}
		}
	}

	public static void removeCoins(String name, Integer coins) {
		try {
			Main.getInstance().getFileRepository().getCoinsConfiguration().load(Main.getInstance().getFileRepository().getCoinsFile());
		} catch (Exception localException) {
		}

		if ((fileExist()) && (Main.getInstance().getFileRepository().getCoinsConfiguration().contains("coins." + name))) {
			Main.getInstance().getFileRepository().getCoinsConfiguration().set("coins." + name, Main.getInstance().getFileRepository().getCoinsConfiguration().getInt("coins." + name) - coins);
			try {
				Main.getInstance().getFileRepository().getCoinsConfiguration().save(Main.getInstance().getFileRepository().getCoinsFile());
			} catch (Exception localException1) {
			}
		}
	}
}