package net.punoxdev.essentialsreloaded.events;

import net.punoxdev.essentialsreloaded.*;
import net.punoxdev.essentialsreloaded.api.CoinsAPI;
import net.punoxdev.essentialsreloaded.api.CommandAPI;
import net.punoxdev.essentialsreloaded.api.MessageAPI;
import net.punoxdev.essentialsreloaded.commands.BroadcastCMD;
import net.punoxdev.essentialsreloaded.commands.ChatClearCMD;
import net.punoxdev.essentialsreloaded.commands.FeedCMD;
import net.punoxdev.essentialsreloaded.commands.FlyCMD;
import net.punoxdev.essentialsreloaded.commands.GiveCMD;
import net.punoxdev.essentialsreloaded.commands.GameModeCMD;
import net.punoxdev.essentialsreloaded.commands.HealCMD;
import net.punoxdev.essentialsreloaded.commands.HomeCMD;
import net.punoxdev.essentialsreloaded.commands.InspectorCMD;
import net.punoxdev.essentialsreloaded.commands.InvseeCMD;
import net.punoxdev.essentialsreloaded.commands.ItemCMD;
import net.punoxdev.essentialsreloaded.commands.KickCMD;
import net.punoxdev.essentialsreloaded.commands.SetHomeCMD;
import net.punoxdev.essentialsreloaded.commands.SetWarpCMD;
import net.punoxdev.essentialsreloaded.commands.TeleportCMD;
import net.punoxdev.essentialsreloaded.commands.TeleportOtherCMD;
import net.punoxdev.essentialsreloaded.commands.VanishCMD;
import net.punoxdev.essentialsreloaded.commands.WarpCMD;
import net.punoxdev.essentialsreloaded.commands.WarpsCMD;
import net.punoxdev.essentialsreloaded.metrics.DevUpdateChecker;
import net.punoxdev.essentialsreloaded.metrics.MainUpdateChecker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

import javax.xml.soap.Text;

import net.punoxdev.essentialsreloaded.repositories.FileRepository;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.Team;

public class EventListener implements Listener {

	private Main main = Main.getInstance();

	private FileRepository fileRepository = main.getFileRepository();

	private String prefix = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.Prefix");

	String JoinMessage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.JoinListener.JoinMessage");
	String LeaveMessage = MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.JoinListener.LeaveMessage");

	@EventHandler
	public <DisplayNameData> void onJoin(PlayerJoinEvent e) {
		final Player p = e.getPlayer();
		
		for(Player player : VanishCMD.hide) {
			for(Player all : Bukkit.getOnlinePlayers()) {
				all.hidePlayer(player);
			}	
			player.sendMessage(this.prefix.replaceAll("&", "§") + "§7§lYou were automatically §e§lhidden §7§lbecause the player §e§l" + p.getName() + " §7§lentered the server§7§l...");
		}

		if (p.getName().equalsIgnoreCase("Punox") || p.getName().equalsIgnoreCase("OfficialPunox")) {
			p.sendMessage("§a§lPunoxCloud §7§l§7» §7Dieses §e§lNetzwerk §r§7verwendet \n §e§lEssentialsREL §7§l(§c§l §b§l" + Main.getCurrentVersion() + " §7§l)" + "\n" + "§8§m---------------[§r §6§lAdmin Overview §8§m]---------------");
			
			// Sending activated commands to admin user
			String namesOfActivatedCommands = new String();
			for(String commandName : CommandAPI.getNamesOfActivatedCommands(main.getCommandRepository().getCommands())) {
				namesOfActivatedCommands += "§a" + commandName + "§8, ";
			}
			
			// Sending disabled commands to admin user
			String namesOfDisabledCommands = new String();
			for(String commandName : CommandAPI.getNamesOfDisabledCommands(main.getCommandRepository().getCommands())) {
				namesOfDisabledCommands += "§4" + commandName + "§8, ";
			}
			
			MessageAPI.sendMessageToPlayer(p, namesOfActivatedCommands);
			MessageAPI.sendMessageToPlayer(p, "");
			MessageAPI.sendMessageToPlayer(p, namesOfDisabledCommands);
	
			//if (MainUpdateChecker.isUpdate()) {
			//	p.sendMessage(this.prefix.replaceAll("&", "§") + "§e§la new version §e§lis available §8(§b§l"
			//			+ MainUpdateChecker.check + " §8)");
			//	p.sendMessage("§e§lLink §7» §5§l§ohttps://goo.gl/cGdQZX §8(§7<-- §e§oKlick§8)");
			//}

			p.sendMessage("§8§m------------------------------------------------");
		}

		if (p.hasPermission("*")) {
	        MainUpdateChecker.check();
	        DevUpdateChecker.check();
			if (MainUpdateChecker.isUpdate()) {
				p.sendMessage(this.prefix.replaceAll("&", "§") + "§e§la new version §e§lis available §8( §b§l"
						+ MainUpdateChecker.check + " §8)");
				p.sendMessage("§e§lLink §7» §5§l§ohttps://goo.gl/cGdQZX §8(§7<-- §e§oKlick§8)");
			}
			
			if (DevUpdateChecker.isUpdate()) {
				p.sendMessage(this.prefix.replaceAll("&", "§") + "§e§la new §4§lDEV - VERSION §e§lis available §8( §b§l"
						+ DevUpdateChecker.check + " §8)");
				p.sendMessage("§e§lLink §7» §5§l§ohttps://goo.gl/gJRCaG §8(§7<-- §e§oKlick§8)");
			}
		}
		
		if(main.getFileRepository().getSettingsConfiguration().getBoolean("settings.toggle.Join & Leave Messages")) {
			e.setJoinMessage(MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.JoinListener.JoinMessage").replaceAll("%player%", p.getName()));
		}
		
		try {
			main.getFileRepository().getCoinsConfiguration().load(main.getFileRepository().getCoinsFile());
		} catch (FileNotFoundException e1) {} catch (IOException e1) {} catch (InvalidConfigurationException e1) {}
	
		CoinsAPI.createUser(p.getName(), main.getFileRepository().getSettingsConfiguration().getInt("settings.setup.PaymentSystem.StartCoins"));
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		if(main.getFileRepository().getSettingsConfiguration().getBoolean("settings.toggle.Join & Leave Messages")) {
			e.setQuitMessage(MessageAPI.getMessageFromConfig(this.fileRepository.getMessagesConfiguration(), "messages.JoinListener.LeaveMessage").replaceAll("%player%", p.getName()));
		}
		FlyCMD.checkfly.remove(p);
	}

}
