package net.punoxdev.essentialsreloaded.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;

public class MySQL {
	
	private String host;
	private String username;
	private String password;
	private String database;
	private int port;
	private static Connection connection;
	
	public MySQL(String host, String username, String password, String database, int port) {
		this.host = host;
		this.username = username;
		this.password = password;
		this.database = database;
		this.port = port;
		connect();
	}
	
	public void connect() {
		if(!instanceRunning()) {
			try {
				 connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?autoReconnect=true", this.username, this.password);
					Bukkit.getConsoleSender().sendMessage("§a§lEssentialsREL §7§l§7» §csuccessfully connected to database §e§ljdbc:mysql§7§l://§b§l" + this.host + "§7§l:§b§l" + this.port + "§7§l/§c§l" + this.database + "§7§l/");
			}catch(SQLException exception) {
				Bukkit.getConsoleSender().sendMessage("§a§lEssentialsREL §7§l§7» §c§lconnection to database §e§ljdbc:mysql§7§l://§b§l" + this.host + "§7§l:§b§l" + this.port + "§7§l/§c§l" + this.database + "§7§l/ " + "§4§lFAILED");
			};
		}
	}
	
	public void disconnect() {
		if(instanceRunning()) {
			try {
				connection.close();
			}catch(SQLException exception) {}
		}
	}
	
	public boolean instanceRunning() {
		return connection != null;
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public void update(String query) {
		try {
			PreparedStatement ps = connection.prepareStatement(query);
			ps.close();
		}catch(SQLException exception) {}
	}
	
	public ResultSet getResult(String query) {
		try {
			PreparedStatement ps = connection.prepareStatement(query);
			return ps.executeQuery();
		}catch(SQLException exception) {}
		return null;
	}

}
